<?php
	ob_start();
	session_start();
	include_once("./includes/connection.php");
	include_once("./includes/functions.php");
	include_once "loginchk.inc.php";

?>
<!DOCTYPE html>

<html>
<head>
<script src="includes/jquery-1.9.1.min.js" type="text/javascript"></script>
<?php include_once('header.php'); ?>
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
 <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
   <!-- Main Header -->
  <?php include_once('topbar.php'); ?>
  <!-- Left side column. contains the logo and sidebar -->
  
 <?php include_once('sidebar.php'); ?>
  <!-- Content Wrapper. Contains page content -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Import CSV</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Import CSV</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">


      <div class="row">
        <div class="col-xs-12">
          <div class="box">
				<div class="box-header">
					<h3 class="box-title">Import CSV</h3>
				</div>
            <!-- /.box-header -->
            <div class="box-body">
    <div id="wrap">
        <div class="container">
            <div class="row">

                <form class="form-horizontal" action="function_export.php" method="post" name="upload_excel" enctype="multipart/form-data">
                    <fieldset>
                        <!-- File Button -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="filebutton">Select File</label>
                            <div class="col-md-4">
                                <input type="file" name="file" id="file" class="input-large">
                            </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="singlebutton">Import data</label>
                            <div class="col-md-4">
                                <button type="submit" id="submit" name="Import" class="btn btn-primary button-loading" data-loading-text="Loading...">Import</button>
                            </div>
                        </div>

                    </fieldset>
                </form>

            </div>

        </div>
    </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
   <?php include_once('footer.php');?>
  <div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#txtdoj').datepicker();
	$('#txtfdov').datepicker();
	$('#example1').dataTable( {
		"ordering": false,
		scrollX : true,
        scrollCollapse : true,
		responsive: true,
        autoWidth: false,
  "columnDefs": [
		{ "width": "4%", "targets": [4,5,6,7,8] },
		{ "width": "20%", "targets": [0,1,2,3] },
	]
	});
  });
   $(document).ready(function() {
	$('a[data-confirm]').click(function(ev) {
		var href = $(this).attr('href');
		if (!$('#dataConfirmModal').length) {
			$('body').append('<div id="dataConfirmModal" class="modal" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button><h3 id="dataConfirmLabel">Please Confirm</h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button><a class="btn btn-primary" id="dataConfirmOK">OK</a></div></div>');
		} 
		$('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
		$('#dataConfirmOK').attr('href', href);
		$('#dataConfirmModal').modal({show:true});
		return false;
	});
});
</script>
<script type="text/javascript">

function selectCategory(c_id){

	if(c_id!=""){
		loadData('category',c_id);
		//loadData('subcategory',c_id);
	}else{
		$("#category_dropdown").html("<option value=''>Select Animal</option>");
	}
}

function selectSubCategory(c_id){

	if(c_id!=""){
		loadData('subcategory',c_id);
	}else{
		$("#subcategory_dropdown").html("<option value=''>Select Breed</option>");		
	}
}

function loadData(loadType,loadId){
	var dataString = 'loadType='+ loadType +'&loadId='+ loadId;
	$("#"+loadType+"_loader").show();
    $("#"+loadType+"_loader").fadeIn(400).html('<img src="images/loader.gif" style="padding: 5px; margin-left: 5px;" />');
	$.ajax({
		type: "POST",
		url: "loadCategory.php",
		data: dataString,
		cache: false,
		success: function(result){
			$("#"+loadType+"_loader").hide();
			$("#"+loadType+"_dropdown").html(result);  
		}
	});
}
</script>
</body>
</html>