<?php
	ob_start();
	session_start();
	include_once "loginchk.inc.php";
	$a_name	= $_SESSION['a_name']; 
	$a_id	= $_SESSION['a_id'];
	include_once("./includes/connection.php");
	include_once("./includes/functions.php");
	include_once "loginchk.inc.php";
			
	if(isset($_POST['subAdd']))
	{	
	$adtime = time();
	array_filter($_POST, 'trim_value');
		$postfilter =array(
		'txtcategory'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
		'txtparent'     		=> array('filter' => FILTER_SANITIZE_NUMBER_INT, 'flags' => ''),
		);
		$revised_post_array = filter_var_array($_POST, $postfilter);  
		$revised_post_array = sanitizearray($revised_post_array, $mysqli);
		

        $qry1_service="INSERT INTO p_category(c_name,c_parent,c_status,c_date) VALUES('".$revised_post_array['txtcategory']."','".$revised_post_array['txtparent']."','1','$adtime')"; 
		$mysqli->query($qry1_service) or die('Error, query failed'); 
		header("location:manage_category.php");	
		}	
	?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('header.php'); ?>
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <?php include_once('topbar.php'); ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('sidebar.php'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Species / Animal / Breed</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Species / Animal / Breed</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="col-md-12">
        <div class="box box-primary collapsed-box">
          <!---->
          <div class="box-header with-border">
            <h3 class="box-title">Add Species / Animal / Breed</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
		  <?php $categoryList = fetchCategoryTree(); ?>
          <!-- form start -->
          <form class="form-horizontal" action=""  method="post" name="frm1" enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-4 control-label">Category (Animal..)</label>
                <div class="col-sm-8">
				  <select class="form-control" name="txtparent">
				<option value="0">None</option>
				<?php foreach($categoryList as $sl) { ?>
				<option value="<?php echo $sl["c_id"] ?>" <?php if($sl["c_id"]=="2") echo " selected "; ?>><?php echo $sl["c_name"]; ?></option>
				<?php } ?>
				</select>
                </div>
              </div>
			  <div class="form-group">
                <label class="col-sm-4 control-label">Sub Category (Breed..)</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="txtcategory"  placeholder="Enter Species / Animal / Breed">
                </div>
              </div>
              
             

              
              
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" name="subAdd" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage Species / Animal / Breed</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                  <tr>
                   	<th>Species / Animal / Breed</th>
                    <th>Edit</th>
					<th>Status</th>
                    <th>Delete</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
					//$qry=$mysqli->query("SELECT * FROM p_category ");
					
					//while($sls = mysqli_fetch_array($qry)){
					foreach($categoryList as $sl){
					?>
                  <tr>
                    <td><?php echo $sl['c_name'];?></td>
                    <td><a href="edit_category.php?id=<?php echo $sl['c_id'] ?>" title="Edit" style="color: #444;"><i class="fa fa-pencil-square" style="font-size: 17px;"></i></a></td>
					<td><?php if($sl['c_status']=='1')  {?>
                      <a href="status_category.php?id=<?php echo $sl['c_id'] ?>&act=0" title="Make it Inactive" style="color: green;"><i class="fa fa-check-circle" style="font-size: 17px;"></i></a>
                      <?php } else {?>
                      <a href="status_category.php?id=<?php echo $sl['c_id'] ?>&act=1" title="Make it Active" style="color: red;"><i class="fa fa-times-circle" style="font-size: 17px;"></i></a>
                      <?php } ?>
                    </td>
                    <td><a href="delete_category.php?id=<?php echo $sl['c_id'] ?>" onClick="return confirm('Are you sure you want to delete?');" title="Delete" style="color: #444;"><i class="fa fa-trash" style="font-size: 17px;"></i></a></td>
                  </tr>
                  <?php }
					?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include_once('footer.php');?>
  <div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<!-- page script -->
<script>
  $(function () {
	$('#example1').dataTable( {
		"ordering": false,
		scrollX : true,
        scrollCollapse : true,
		responsive: true,
        autoWidth: false,
  "columnDefs": [
		{ "width": "4%", "targets": [1,2,3] },
		{ "width": "40%", "targets": [0] },
	]
	});
  });
   $(document).ready(function() {
	$('a[data-confirm]').click(function(ev) {
		var href = $(this).attr('href');
		if (!$('#dataConfirmModal').length) {
			$('body').append('<div id="dataConfirmModal" class="modal" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button><h3 id="dataConfirmLabel">Please Confirm</h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button><a class="btn btn-primary" id="dataConfirmOK">OK</a></div></div>');
		} 
		$('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
		$('#dataConfirmOK').attr('href', href);
		$('#dataConfirmModal').modal({show:true});
		return false;
	});
});
</script>
</body>
</html>
