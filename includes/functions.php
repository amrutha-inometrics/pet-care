<?php
function sanitize($input, $con){
    if(get_magic_quotes_gpc()){
        $input = trim($input); // get rid of white space left and right
		$input = stripslashes($input);
        $input = htmlentities($input); // convert symbols to html entities
        return $input;
    } else {
        $input = htmlentities($input); // convert symbols to html entities
        $input = addslashes($input); // server doesn't add slashes, so we will add them to escape ',",\,NULL
        $input = mysqli_real_escape_string($con, $input); // escapes \x00, \n, \r, \, ', " and \x1a
        return $input;
    }
}

function sanitizearray(&$array, $con) {
    $clean = array();
    foreach($array as $key => &$value ) {
        $value = trim(strip_tags($value));
        if (get_magic_quotes_gpc()) {
            $data = stripslashes($value);
        }else{
			$data = addslashes($value);
		}
        $data = mysqli_real_escape_string($con, $data);
		$clean[$key]=$data;
    }
	return $clean;
}

function trim_value(&$value)
{
    $value = trim($value);
}

function runSQL($rsql) {
	global $host,$username,$password,$database;
	$connect = mysql_connect($host,$username,$password) or die ("Error: could not connect to database");
	$db = mysql_select_db($database);
	$result = mysql_query($rsql) or die ('test'); 
	return $result;
	mysql_close($connect);
}

function resizesmimage($ext='',$source, $destination, $filename,$w1,$w2,$h1=0,$h2=0) { 
$name = $ext.time().".".end(explode(".", $filename)); 
$thumbsize=$w1; 
$thumbsize1=$w2; 
$imgfile = $source; 
list($width, $height) = getimagesize($imgfile); 
if($h1>0 && $h2>0){
    $newwidth = $thumbsize;
    $newheight = $h1;
    $newwidth1 = $thumbsize1; 
    $newheight1 = $h2;
}else{
    $imgratio=$width/$height; 
    if ($imgratio>1){
    $newwidth = $thumbsize;
    $newheight = $thumbsize/$imgratio;
    $newwidth1 = $thumbsize1; 
    $newheight1 = $thumbsize1/$imgratio; 
    }else{
    $newheight = $thumbsize;
    $newwidth = $thumbsize*$imgratio;
    $newheight1 = $thumbsize1;
    $newwidth1 = $thumbsize1*$imgratio;
    }
}

if (function_exists('imagecreatetruecolor')) { 
$newimage = ImageCreateTrueColor($newwidth,$newheight); 
$newimage1 = ImageCreateTrueColor($newwidth1,$newheight1); 
} else { 
$newimage = imagecreate($newwidth,$newheight); 
$newimage1 = imagecreate($newwidth1,$newheight1); 
} 
 
if(preg_match("/.jpg/i", "$imgfile")) 
{ 
$format = 'image/jpeg'; 
} 
if (preg_match("/.gif/i", "$imgfile")) 
{ 
$format = 'image/gif'; 
} 
if(preg_match("/.png/i", "$imgfile")) 
{ 
$format = 'image/png'; 
} 
 
switch($format) 
{
case 'image/jpeg':
$source = imagecreatefromjpeg($imgfile);
imagecopyresampled ($newimage, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height); 
imagecopyresampled ($newimage1, $source, 0, 0, 0, 0, $newwidth1, $newheight1, $width, $height); 
imagejpeg($newimage,$destination."/".$name,100); 
imagejpeg($newimage1,$destination."/small/".$name,100);
break;
case 'image/gif';
$source = imagecreatefromgif($imgfile);
imagecolortransparent($newimage, imagecolorallocatealpha($newimage, 0, 0, 0, 127));
imagealphablending($newimage, false);
imagesavealpha($newimage, true);
imagecolortransparent($newimage1, imagecolorallocatealpha($newimage1, 0, 0, 0, 127));
imagealphablending($newimage1, false);
imagesavealpha($newimage1, true);
imagecopyresampled ($newimage, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height); 
imagecopyresampled ($newimage1, $source, 0, 0, 0, 0, $newwidth1, $newheight1, $width, $height); 
imagegif($newimage,$destination."/".$name); 
imagegif($newimage1,$destination."/small/".$name);
break;
case 'image/png':
$source = imagecreatefrompng($imgfile);
imagecolortransparent($newimage, imagecolorallocatealpha($newimage, 0, 0, 0, 127));
imagealphablending($newimage, false);
imagesavealpha($newimage, true);
imagecolortransparent($newimage1, imagecolorallocatealpha($newimage1, 0, 0, 0, 127));
imagealphablending($newimage1, false);
imagesavealpha($newimage1, true);
imagecopyresampled ($newimage, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height); 
imagecopyresampled ($newimage1, $source, 0, 0, 0, 0, $newwidth1, $newheight1, $width, $height); 
imagepng($newimage,$destination."/".$name); 
imagepng($newimage1,$destination."/small/".$name);
break;
}
 
$src="files/temp/".$filename; 
unlink($src);     
return $name;  
} 

function resizeimage($source, $destination, $filename, $w1, $h1){
$name = time().".".strtolower(end(explode(".", $filename)));
$thumbsize=$w1;
$imgfile = $source;
list($width, $height) = getimagesize($imgfile);
if($h1>0){
    $newwidth = $thumbsize;
    $newheight = $h1;
}else{
    $imgratio=$width/$height;
    if ($imgratio>1){
    $newwidth = $thumbsize;
    $newheight = $thumbsize/$imgratio;
    }else{
    $newheight = $thumbsize;
    $newwidth = $thumbsize*$imgratio;
    }
}

if (function_exists('imagecreatetruecolor')) {
$newimage = ImageCreateTrueColor($newwidth,$newheight);
} else {
$newimage = imagecreate($newwidth,$newheight);
}
if(preg_match("/.jpg/i", "$imgfile"))
{
$format = 'image/jpeg';
}
if (preg_match("/.gif/i", "$imgfile"))
{
$format = 'image/gif';
}
if(preg_match("/.png/i", "$imgfile"))
{
$format = 'image/png';
}
switch($format)
{
case 'image/jpeg':
$source = imagecreatefromjpeg($imgfile);
imagecopyresampled ($newimage, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
imagejpeg($newimage,$destination."/".$name,100);
break;
case 'image/gif';
$source = imagecreatefromgif($imgfile);
imagecolortransparent($newimage, imagecolorallocatealpha($newimage, 0, 0, 0, 127));
imagealphablending($newimage, false);
imagesavealpha($newimage, true);
imagecopyresampled ($newimage, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
imagegif($newimage,$destination."/".$name);
break;
case 'image/png':
$source = imagecreatefrompng($imgfile);
imagecolortransparent($newimage, imagecolorallocatealpha($newimage, 0, 0, 0, 127));
imagealphablending($newimage, false);
imagesavealpha($newimage, true);
imagecopyresampled ($newimage, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
imagepng($newimage,$destination."/".$name);
break;
}

$src="files/temp/".$filename;
unlink($src);	
return $name; 
}

function fetchCategoryTree($parent = 0, $spacing = '', $user_tree_array = '') {
global $mysqli;
  if (!is_array($user_tree_array))
    $user_tree_array = array();

  $sql = "SELECT c_id, c_name, c_parent, c_status FROM p_category WHERE 1 AND c_parent = $parent ORDER BY c_id ASC";
  $query = $mysqli->query($sql);
  if (mysqli_num_rows($query) > 0) {
    while ($row = mysqli_fetch_object($query)) {
      $user_tree_array[] = array("c_id" => $row->c_id,"c_status" => $row->c_status, "c_name" => $spacing . $row->c_name);
      $user_tree_array = fetchCategoryTree($row->c_id, $spacing . '&nbsp;&nbsp;&nbsp;|-&nbsp;', $user_tree_array);
    }
  }
  return $user_tree_array;
}

function fetchCategoryTreeList($parent = 0, $user_tree_array = '') {
global $mysqli;
    if (!is_array($user_tree_array))
    $user_tree_array = array();

  $sql = "SELECT c_id, c_name, c_parent, c_status FROM p_category WHERE 1 AND c_parent = $parent ORDER BY c_id ASC";
  $query = mysqli_query($sql);
  if (mysqli_num_rows($query) > 0) {
     $user_tree_array[] = "<ul>";
    while ($row = mysqli_fetch_object($query)) {
	  $user_tree_array[] = "<li>". $row->name."</li>";
      $user_tree_array = fetchCategoryTreeList($row->cid, $user_tree_array);
    }
	$user_tree_array[] = "</ul>";
  }
  return $user_tree_array;
}
function fetchCategoryChilds($parent = 0, $user_tree_array = '') {
global $mysqli;
  if (!is_array($user_tree_array))
    $user_tree_array = array();

  $sql = "SELECT c_id FROM p_category WHERE c_parent = $parent";
  $query = $mysqli->query($sql);
  if (mysqli_num_rows($query) > 0) {
    while ($row = mysqli_fetch_object($query)) {
      $user_tree_array[] = array("c_id" => $row->c_id);
      $user_tree_array = fetchCategoryChilds($row->c_id,$user_tree_array);
    }
  }
  return $user_tree_array;
}

function _date_range_limit($start, $end, $adj, $a, $b, $result)
{
    if ($result[$a] < $start) {
        $result[$b] -= intval(($start - $result[$a] - 1) / $adj) + 1;
        $result[$a] += $adj * intval(($start - $result[$a] - 1) / $adj + 1);
    }

    if ($result[$a] >= $end) {
        $result[$b] += intval($result[$a] / $adj);
        $result[$a] -= $adj * intval($result[$a] / $adj);
    }

    return $result;
}

function _date_range_limit_days($base, $result)
{
    $days_in_month_leap = array(31, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    $days_in_month = array(31, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    _date_range_limit(1, 13, 12, "m", "y", $base);

    $year = $base["y"];
    $month = $base["m"];

    if (!$result["invert"]) {
        while ($result["d"] < 0) {
            $month--;
            if ($month < 1) {
                $month += 12;
                $year--;
            }

            $leapyear = $year % 400 == 0 || ($year % 100 != 0 && $year % 4 == 0);
            $days = $leapyear ? $days_in_month_leap[$month] : $days_in_month[$month];

            $result["d"] += $days;
            $result["m"]--;
        }
    } else {
        while ($result["d"] < 0) {
            $leapyear = $year % 400 == 0 || ($year % 100 != 0 && $year % 4 == 0);
            $days = $leapyear ? $days_in_month_leap[$month] : $days_in_month[$month];

            $result["d"] += $days;
            $result["m"]--;

            $month++;
            if ($month > 12) {
                $month -= 12;
                $year++;
            }
        }
    }

    return $result;
}

function _date_normalize($base, $result)
{
    $result = _date_range_limit(0, 60, 60, "s", "i", $result);
    $result = _date_range_limit(0, 60, 60, "i", "h", $result);
    $result = _date_range_limit(0, 24, 24, "h", "d", $result);
    $result = _date_range_limit(0, 12, 12, "m", "y", $result);

    $result = _date_range_limit_days($base, $result);

    $result = _date_range_limit(0, 12, 12, "m", "y", $result);

    return $result;
}

/**
 * Accepts two unix timestamps.
 */
function _date_diff($one, $two)
{
    $invert = false;
    if ($one > $two) {
        list($one, $two) = array($two, $one);
        $invert = true;
    }

    $key = array("y", "m", "d");
    $a = array_combine($key, array_map("intval", explode(" ", date("Y m d", $one))));
    $b = array_combine($key, array_map("intval", explode(" ", date("Y m d", $two))));

    $result = array();
    $result["y"] = $b["y"] - $a["y"];
    $result["m"] = $b["m"] - $a["m"];
    $result["d"] = $b["d"] - $a["d"];
    //$result["h"] = $b["h"] - $a["h"];
    //$result["i"] = $b["i"] - $a["i"];
    //$result["s"] = $b["s"] - $a["s"];
    //$result["invert"] = $invert ? 1 : 0;
    $result["days"] = intval(abs(($one - $two)/86400));

    if ($invert) {
        _date_normalize($a, $result);
    } else {
        _date_normalize($b, $result);
    }

    return $result;
}
?>