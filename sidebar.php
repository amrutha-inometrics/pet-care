<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/avatar5.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $_SESSION['a_name']; ?></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
       <?php $cpage = basename($_SERVER['PHP_SELF'], ".php");?>
        <!-- Optionally, you can add icons to the links -->
        <li <?php if($cpage=="home") echo 'class="active"'; ?> ><a href="./home.php"><i class="fa fa-home"></i> <span>Home</span></a></li>
		<li <?php if($cpage=="send_message") echo 'class="active"'; ?>><a href="send_message.php"><i class="fa fa-envelope"></i> <span>Send Message</span></a></li>
		<li <?php if($cpage=="manage_category") echo 'class="active"'; ?>><a href="manage_category.php"><i class="fa fa-list-ol"></i> <span>Manage Animals</span></a></li>
		<li <?php if($cpage=="manage_patient") echo 'class="active"'; ?>><a href="manage_patient.php"><i class="fa fa-bed"></i> <span>Manage Patients</span></a></li>
    <li class="treeview  <?php if($cpage=="invoice") echo ' active  '; ?>"><a href="invoice.php"><i class="fa fa-edit"></i> <span>Invoice</span> <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
    <ul class="treeview-menu"><li <?php if($cpage=="invoice") echo 'class="active"'; ?>><a href="invoice.php"><i class="fa fa-edit"></i> <span>OP Invoice</span></a></li>
    <li <?php if($cpage=="invoice_ip") echo 'class="active"'; ?>><a href="invoice_ip.php"><i class="fa fa-edit"></i> <span>IP Invoice</span></a></li>
    <li <?php if($cpage=="invoice_lab") echo 'class="active"'; ?>><a href="invoice_lab.php"><i class="fa fa-edit"></i> <span>LAB Invoice</span></a></li></ul>
    </li>
		<?php if($_SESSION['a_id'] != 4 && $_SESSION['a_id'] != 5){ ?>
			<li><a href="importcsv.php"><i class="fa fa-bed"></i> <span>Import CSV</span></a></li>
			<li><a href="exportcsv.php"><i class="fa fa-bed"></i> <span>Export csv</span></a></li>

		<?php }if($_SESSION['a_id'] == 1){ ?>
			<li <?php if($cpage=="change_pwd") echo 'class="active"'; ?>><a href="change_pwd.php"><i class="fa fa-key"></i> <span>Change Password</span></a></li>
		<?php } ?>
		<li <?php if($cpage=="logout") echo 'class="active"'; ?>><a href="logout.php"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>

        <!--<li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li>-->
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>
