<?php
	ob_start();
	session_start();
	include_once "loginchk.inc.php";
	$a_name	= $_SESSION['a_name']; 
	$a_id	= $_SESSION['a_id'];
	include_once("./includes/connection.php");
	include_once("./includes/functions.php");
		
		array_filter($_GET, 'trim_value');
		$postfilter =array(
				'id'     		=> array('filter' => FILTER_SANITIZE_NUMBER_INT, 'flags' => ''),
				//'cid'     		=> array('filter' => FILTER_SANITIZE_NUMBER_INT, 'flags' => ''),
		);
			$revised_post_array = filter_var_array($_GET, $postfilter);  
			$id 				= $revised_post_array['id'];
			//$cid 				= $revised_post_array['cid']; 
			
		$qry=$mysqli->query("select * from p_category where c_id=$id");
		$rows = mysqli_fetch_object($qry);
		
		if(isset($_POST['subAdd']))
		{	

		$adtime = time();
		$postfilter =array(
		'txtcategory'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
		'txtparent'     		=> array('filter' => FILTER_SANITIZE_NUMBER_INT, 'flags' => ''),
		);
		$revised_post_array = filter_var_array($_POST, $postfilter);  
		$revised_post_array = sanitizearray($revised_post_array, $mysqli);
		
		
		$qry_user="UPDATE  p_category SET c_name = '".$revised_post_array['txtcategory']."', c_parent = '".$revised_post_array['txtparent']."' WHERE c_id = '$id' "; 
		$mysqli->query($qry_user) or die('Error, query failed');
		header ("location:manage_category.php?id=$id");
		
		}	
	?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('header.php'); ?>
<!-- date picker -->
<link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <?php include_once('topbar.php'); ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('sidebar.php'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Main Species / Animal / Breed</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Edit Species / Animal / Breed</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Species / Animal / Breed</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
		  
		  <?php $categoryList = fetchCategoryTree(); ?>
		   
          <form class="form-horizontal" action=""  method="post" name="frm1" enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-4 control-label">Species / Animal / Breed</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="txtcategory"  placeholder="Enter Species / Animal / Breed" value="<?php echo $rows->c_name; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Parent</label>
                <div class="col-sm-8">
				  <select class="form-control" name="txtparent">
				<option value="0">None</option>
				<?php foreach($categoryList as $sl) { ?>
				<option value="<?php echo $sl["c_id"] ?>" <?php if($sl["c_id"]==$rows->c_parent) echo 'selected="selected"';?>><?php echo $sl["c_name"]; ?></option>
				<?php } ?>
				</select>
                </div>
              </div>
             

              
              
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" name="subAdd" class="btn btn-primary">Submit</button>
            </div>
          </form>

			
          
        </div>
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include_once('footer.php');?>
  <div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- date-range-picker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#txtdoj').datepicker();
	$('#example1').dataTable( {
  "columnDefs": [
		{ "width": "5%", "targets": [0,3,4,5,6,7] },
	]
	});
  });
</script>
</body>
</html>
