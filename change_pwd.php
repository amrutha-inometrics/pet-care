<?php
	ob_start();
	session_start();
	include_once "loginchk.inc.php";
	$a_name	= $_SESSION['a_name']; 
	$a_id	= $_SESSION['a_id'];
	include_once("./includes/connection.php");
	include_once("./includes/functions.php");
	$a_name		= $_SESSION['a_name']; 
	$a_id		= $_SESSION['a_id'];
	$msg	 	= isset($_REQUEST['msg'])	?	$_REQUEST['msg']	:	"";
	 if(isset($_POST['subAddD']))
		{
		array_filter($_POST, 'trim_value');
		$postfilter =array(
		'np'   	 => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
		'op'    => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
		);
		$revised_post_array = filter_var_array($_POST, $postfilter);  
		$newpwd 		= md5($revised_post_array['np']); 
		$oldpwd 		= md5($revised_post_array['op']); 
			
		$sqol="select * from p_admin where admin_pwd='$oldpwd' and admin_id='$a_id'"; 
		$qry		= $mysqli->query($sqol);
		$num		= mysqli_num_rows($qry); 
		if($num==1)
				{
				 $sqol2="update p_admin set admin_pwd='$newpwd' where admin_id='$a_id'";
				 $upqry = $mysqli->query($sqol2);
					if($upqry)
					{
						$msg="Password Updated!";
					header("location:logout.php?msg=$msg");
					}
					else
					{
						$msg="Unknown problem,Please try later!";
						header("location:change_pwd.php?msg=$msg");
					}
				}
			else
				{
					$msg="Incorrect Current Password!";
					header("location:change_pwd.php?msg=$msg");
				}
		}
?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('header.php'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <?php include_once('topbar.php'); ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('sidebar.php'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Change Password</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Change Password</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      
      <div class="col-md-12">
        <div class="box box-primary ">
          <div class="box-header with-border">
            <h3 class="box-title">Change Password</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div>
          </div>

          <form class="form-horizontal" action=""  method="post" name="frm1" enctype="multipart/form-data">
            <div class="box-body"><?php if($msg) echo '<h3 class="box-title">'.$msg.'</h3>'; ?>
              <div class="form-group">
                <label class="col-sm-4 control-label">Old Password </label>
                <div class="col-sm-8">
                  <input type="password" name="op" class="form-control"  placeholder="Old Password">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">New Password</label>
                <div class="col-sm-8">
                   <input type="password" name="np" class="form-control"  placeholder="New Password">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Confirm Password</label>
                <div class="col-sm-8">
                  <input type="password" name="cp" class="form-control"  placeholder="Confirm Password">
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" name="subAddD" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
      
    </section>
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include_once('footer.php');?>
  <div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- date-range-picker -->

<!-- page script -->

</body>
</html>
