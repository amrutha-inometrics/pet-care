<?php
	ob_start();
	session_start();
	include_once("./includes/connection.php");
	include_once("./includes/functions.php");
		include_once "loginchk.inc.php";
		array_filter($_GET, 'trim_value');
		$postfilter =array(
				'id'     		=> array('filter' => FILTER_SANITIZE_NUMBER_INT, 'flags' => ''),
		);
			$revised_post_array = filter_var_array($_GET, $postfilter);  
			$id 				= $revised_post_array['id'];
			
		$qry=$mysqli->query("select * from p_patient where p_id=$id");
		$rows = mysqli_fetch_object($qry);
		$p_image1 = $rows->u_photo;
		
	?>
<!DOCTYPE html>
<html>
<head>
<script src="includes/jquery-1.9.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
$('.ageclass').change(function(){
var dat = new Date();
var curday = dat.getDate();
var curmon = dat.getMonth()+1;
var curyear = dat.getFullYear();
var calday = document.getElementById("selbday").value;
var calmon = document.getElementById("selbmonth").value;
var calyear = document.getElementById("selbyear").value;
	if(curday == "" || curmon=="" || curyear=="" || calday=="" || calmon=="" || calyear=="")
	{
		return false;
	}	
	else
	{
		var curd = new Date(curyear,curmon-1,curday);
		var cald = new Date(calyear,calmon-1,calday);
		var diff =  Date.UTC(curyear,curmon,curday,0,0,0) - Date.UTC(calyear,calmon,calday,0,0,0);
		var dife = datediff(curd,cald);
		var agevalue=dife[0];
	}
        $('#age').val(agevalue);
    });
});
function checkleapyear(datea)
{
	if(datea.getYear()%4 == 0)
	{
		if(datea.getYear()% 10 != 0)
		{
			return true;
		}
		else
		{
			if(datea.getYear()% 400 == 0)
				return true;
			else
				return false;
		}
	}
  return false;
}
function DaysInMonth(Y, M) {
    with (new Date(Y, M, 1, 12)) {
        setDate(0);
        return getDate();
    }
}
function datediff(date1, date2) {
    var y1 = date1.getFullYear(), m1 = date1.getMonth(), d1 = date1.getDate(),
	 y2 = date2.getFullYear(), m2 = date2.getMonth(), d2 = date2.getDate();
    if (d1 < d2) {
        m1--;
        d1 += DaysInMonth(y2, m2);
    }
    if (m1 < m2) {
        y1--;
        m1 += 12;
    }
    return [y1 - y2, m1 - m2, d1 - d2];
}
</script>
<?php include_once('header.php'); ?>
<!-- date picker -->
<link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="col-md-12">
      <div class="box box-primary">
        <h2 align="center">
        The Cochin Pet Hospital
        </h2>
        <h2 align="center">CLINICAL CASE REPORT</h2>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            <?php if($rows->p_photo != ""){?>
            <div class="form-group">
              <label class="col-sm-4 control-label">Photo</label>
              <div class="col-sm-8"> <img src="<?php echo "files/photos/".$rows->p_photo; ?>" height="100" width="100" > </div>
            </div>
            <br>
            <?php } ?>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Date of Joining</label>
              <div class="col-sm-8">
                <input  name="txtdoj" type="text" class="form-control pull-right" id="txtdoj" placeholder="Date of Joining" value="<?php echo date('d-m-Y',$rows->p_date); ?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Patient ID</label>
              <div class="col-sm-8">
                <input type="text" name="txtpatientid" class="form-control"  placeholder="Enter Patient ID" value="<?php echo $rows->p_pid; ?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Registration Fees</label>
              <div class="col-sm-8">
                <input type="text" name="txtregfees" class="form-control"  placeholder="Enter Registration Fees" value="<?php echo $rows->p_regfees; ?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Gender</label>
              <div class="col-sm-8">
                <input type="radio" name="rgender" id="rgender" value="Male" <?php if(!(strcmp($rows->p_gender,"Male"))) {echo "checked=\"checked\"";} ?> >
                Male
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="rgender" id="rgender" value="Female" <?php if(!(strcmp($rows->p_gender,"Female"))) {echo "checked=\"checked\"";} ?> >
                Female </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Species</label>
              <div class="col-sm-8">
                <?php
						 $asso = $mysqli->query("SELECT c_name FROM p_category where c_id =".$rows->p_species);
						 $ass = $asso->fetch_object();
						 $species = $ass->c_name;
					?>
                <input type="text" name="txtregfees" class="form-control"  placeholder="Enter Registration Fees" value="<?php echo $species;?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Animal</label>
              <div class="col-sm-8">
                <?php
						 $assa = $mysqli->query("SELECT c_name FROM p_category where c_id =".$rows->p_animal);
						 $assa = $assa->fetch_object();
						 $animal = $assa->c_name;
					?>
                <input type="text" name="txtregfees" class="form-control"  placeholder="Enter Registration Fees" value="<?php echo $animal;?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Breed</label>
              <div class="col-sm-8">
                <?php
						 $assb = $mysqli->query("SELECT c_name FROM p_category where c_id =".$rows->p_breed);
						 $assb = $assb->fetch_object();
						 $breed = $assb->c_name;
					?>
                <input type="text" name="txtregfees" class="form-control"  placeholder="Enter Registration Fees" value="<?php echo $breed;?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label"> Name </label>
              <div class="col-sm-8">
                <input type="text" name="txtname" class="form-control"  placeholder="Enter Name" value="<?php echo $rows->p_name; ?>">
              </div>
            </div>
            <?php 
			  if($rows->p_dob != ''){
			  	$dobirth =  date('d-m-Y',$rows->p_dob); 
				$cage = getage($dobirth);
			}
			  ?>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label"> Date of Birth and Age </label>
              <div class="col-sm-8">
                <input type="text" name="txtname" class="form-control"  placeholder="Enter Name" value="<?php echo $dobirth.' , '.$cage.' years'; ?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Type of Vaccination</label>
              <div class="col-sm-8">
                <input type="radio" name="tovaccination" id="tovaccination" value="Puppy Vaccination" <?php if(!(strcmp($rows->p_puppytype,"Puppy Vaccination"))) {echo "checked=\"checked\"";} ?> >
                Puppy Vaccination
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="tovaccination" id="tovaccination" value="Adult Vaccination" <?php if(!(strcmp($rows->p_puppytype,"Adult Vaccination"))) {echo "checked=\"checked\"";} ?> >
                Adult Vaccination </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Date of First Vaccination</label>
              <div class="col-sm-8">
                <input name="txtfdov" type="text" class="form-control pull-right" id="txtfdov" placeholder="Date of First Vaccination"  value="<?php echo date('d-m-Y',$rows->p_firstvaccination); ?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Colour</label>
              <div class="col-sm-8">
                <input name="txtcolour" type="text" class="form-control"  placeholder="Enter Colour" value="<?php echo $rows->p_color; ?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Weight</label>
              <div class="col-sm-8">
                <input name="txtweight" type="text" class="form-control"  placeholder="Enter Weight" value="<?php echo $rows->p_wieght; ?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Chip No</label>
              <div class="col-sm-8">
                <input name="txtchipno" type="text" class="form-control"  placeholder="Enter Chip No" value="<?php echo $rows->p_chipno; ?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Health Card No</label>
              <div class="col-sm-8">
                <input name="txthcardno" type="text" class="form-control"  placeholder="Enter Health Card No" value="<?php echo $rows->p_healthcard; ?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Valid</label>
              <div class="col-sm-8">
                <input type="radio" name="valid" id="valid" value="Yes" <?php if(!(strcmp($rows->p_valid,"Yes"))) {echo "checked=\"checked\"";} ?> >
                Yes
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="valid" id="valid" value="No" <?php if(!(strcmp($rows->p_valid,"No"))) {echo "checked=\"checked\"";} ?> >
                No </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Owner Name</label>
              <div class="col-sm-8">
                <input name="txtowner" type="text" class="form-control" placeholder="Enter Name of Owner" value="<?php echo $rows->p_ownername; ?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Address</label>
              <div class="col-sm-8">
                <textarea name="txtaddress" class="form-control "  rows="3" placeholder="Enter Address of Owner"><?php echo $rows->p_address; ?></textarea>
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Pincode</label>
              <div class="col-sm-8">
                <input name="txtpincode" type="text" class="form-control" placeholder="Enter Pincode" value="<?php echo $rows->p_pincode; ?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">State</label>
              <div class="col-sm-8">
                <input name="txtstate" type="text" class="form-control" placeholder="Enter State" value="<?php echo $rows->p_state; ?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Phone Number</label>
              <div class="col-sm-8">
                <input type="text" name="txtphone" class="form-control  col-sm-4"   placeholder="Enter Phone Number" value="<?php echo $rows->p_phone; ?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label class="col-sm-4 control-label">Mobile Number</label>
              <div class="col-sm-8">
                <input type="text" name="txtmobile" class="form-control  col-sm-4"  placeholder="Enter Mobile Number" value="<?php echo $rows->p_mobile; ?>">
              </div>
            </div>
            <div class="form-group col-md-6 col-sm-6">
              <label for="exampleInputEmail1" class="col-sm-4 control-label">Email address</label>
              <div class="col-sm-8">
                <input name="txtemail" type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Email address" value="<?php echo $rows->p_email; ?>">
              </div>
            </div>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
    <!-- /.row -->
  </section>
</div>
<!-- /.content-wrapper -->
<!-- Main Footer -->
<div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- date-range-picker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#txtdoj').datepicker();
	$('#example1').dataTable( {
  "columnDefs": [
		{ "width": "5%", "targets": [0,3,4,5,6,7] },
	]
	});
  });
</script>
<script language="javascript" type="text/javascript">
		window.onload = function() {
        document.body.offsetHeight;
       var is_chrome = function () { return Boolean(window.chrome); }
		if(is_chrome) 
		{
		   window.print();
		   setTimeout(function(){window.close();}, 10000); 
		   //give them 10 seconds to print, then close
		}
		else
		{
		   window.print();
		   window.close();
		}
        }
</script>
</body>
</html>
