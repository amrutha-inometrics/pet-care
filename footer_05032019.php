<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      All rights reserved.
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo date("Y");?>. cochinpethospital</strong>
  </footer>