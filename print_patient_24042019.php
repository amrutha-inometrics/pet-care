<?php
ini_set('display_errors', 'Off');  
	ob_start();
	session_start();
	include_once("./includes/connection.php");
	include_once("./includes/functions.php");
		include_once "loginchk.inc.php";
		array_filter($_GET, 'trim_value');
		$postfilter =array(
				'id'     		=> array('filter' => FILTER_SANITIZE_NUMBER_INT, 'flags' => ''),
		);
			$revised_post_array = filter_var_array($_GET, $postfilter);  
			$id 				= $revised_post_array['id'];
			
		$qry=$mysqli->query("select * from p_patient where p_id=$id");
		$rows = mysqli_fetch_object($qry);
				$species_array = explode(',',$rows->p_vaccination);	
		/* print_r($species_array);exit; */
		$vaccination=$species_array[1]." ".$species_array[0].",".$rows->p_treatment; 
		
	?>
<!DOCTYPE html>
<html>
<head>
<script src="includes/jquery-1.9.1.min.js" type="text/javascript"></script>

<style>

    h1 {
        color: #444;
        font-family: helvetica;
        font-size: 20px;
		text-transform: uppercase;
    }
    table.first {
        color: #444;
        font-family: helvetica;
        border: 3px solid #fff;
        background-color: #fff;
    }
    td {
        border: 2px solid #fff;
        background-color: #fff;
    }
	.highlight{
		color: #222;
		font-weight:bold;
	}
	@media print 
{
  @page { margin-top: 0; }
  body  { margin-top: 1.6cm; }
}
}

</style>
<?php include_once('header.php'); ?>
<!-- date picker -->
<link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div style="text-align:center;margin:20px 0px;">
	<div style="display:inline-block;">
		<div>
			<img  src="images/logo.png">
		</div>
		<div>
			<h2>Clinical Case Report</h2>
		</div>
		<div>
			<h3>Patient and Owner Data</h3>
		</div>
	</div>
</div>


	<div style="width:100%;">
		<div style="width:50%;float:left;padding-left:20px;">
          <table class="first" border="0" nobr="true" style="width:100%;">
			<tr height="40px">
              <td width="180">Patient Name</td>
              <td colspan="3" class="highlight"><?php echo $rows->p_name;?></td>
            </tr>
            <!--<tr height="40px">
              <td width="180">Registration Fees</td>
              <td colspan="2" class="highlight"><?php /* echo $rows->p_regfees; */ ?></td>
            </tr>-->
            <tr height="40px">
              <td width="180">Gender</td>
              <td colspan="2" class="highlight"><?php echo $rows->p_gender; ?></td>
            </tr>
            <tr height="40px">
              <td width="180">Species</td>
              <td colspan="2" class="highlight"><?php $asso = $mysqli->query("SELECT c_name FROM p_category where c_id =".$rows->p_species);
						 $ass = $asso->fetch_object();
						 echo $species = $ass->c_name;?></td>
            </tr>
            <tr height="40px">
              <td width="180">Animal</td>
              <td colspan="2" class="highlight"><?php $assa = $mysqli->query("SELECT c_name FROM p_category where c_id =".$rows->p_animal);
						 $assa = $assa->fetch_object();
						echo $animal = $assa->c_name;?></td>
            </tr>
            <tr height="40px">
              <td width="180">Breed</td>
              <td colspan="3" class="highlight"><?php $assb = $mysqli->query("SELECT c_name FROM p_category where c_id =".$rows->p_breed);
						 $assb = $assb->fetch_object();
						 echo $breed = $assb->c_name;?></td>
            </tr>
            <tr  height="40px">
              <td width="180">DOB</td>
              <td colspan="3" class="highlight"><?php if($rows->p_dob != ''){
				   	$age =_date_diff($rows->p_dob, time());
					$days =  $age[days];
					$years = ($days / 365) ; 
					$years = floor($years); 
					$month = ($days % 365) / 30.5; 
					$month = floor($month); 
					$cage =  $years." year(s), ".$month." month(s)";
					} 
					 echo 	$dobirth =  date('d-m-Y',$rows->p_dob);
					/* echo ", ".$cage; */
			?>
				</td>
            </tr>
            <tr  height="40px">
              <td width="180">AGE</td>
              <td colspan="3" class="highlight"><?php if($rows->p_dob != ''){
				   	$age =_date_diff($rows->p_dob, time());
					$days =  $age[days];
					$years = ($days / 365) ; 
					$years = floor($years); 
					$month = ($days % 365) / 30.5; 
					$month = floor($month); 
					$cage =  $years." year(s), ".$month." month(s)";
					} 
					/*  echo 	$dobirth =  date('d-m-Y',$rows->p_dob); */
					echo $cage;
			?>
				</td>
            </tr>			
			<tr height="40px">
              <td width="180">Type of Vaccination</td>
              <td colspan="2" class="highlight"><?php echo $vaccination?></td>
            </tr>
			<tr height="40px">
              <td width="180">Date of First Vaccination</td>
              <td colspan="2" class="highlight"><?php echo date('d-m-Y',$rows->p_firstvaccination); ?></td>
            </tr>						
            <tr>
			<tr height="40px">
              <td width="180">Colour</td>
              <td colspan="2" class="highlight"><?php echo $rows->p_color; ?></td>
            </tr>						
			<tr height="40px">
              <td width="180">Weight</td>
              <td colspan="2" class="highlight"><?php echo $rows->p_wieght; ?></td>
            </tr>	
          </table>
		</div>
		<div style="width:50%;float:left;">
          <table class="first" border="0" nobr="true" style="width:100%;">
            <tr height="40px">
              <td width="180">Patient ID</td>
              <td colspan="2" class="highlight"><?php echo $rows->p_pid; ?></td>
            </tr>
            <tr height="40px">
              <td width="180">Registration Date</td>
              <td colspan="2" class="highlight"><?php echo date('d-m-Y',$rows->p_date); ?></td>
            </tr>			
            <tr height="40px">
              <td width="180">Owner Name</td>
              <td colspan="2" class="highlight"><?php echo $rows->p_ownername; ?></td>
            </tr>
			<tr height="40px">
              <td width="180">Address</td>
              <td colspan="2" class="highlight"><?php echo $rows->p_address; ?></td>
            </tr>
			<tr height="40px">
              <td width="180">Pincode</td>
              <td colspan="2" class="highlight"><?php echo $rows->p_pincode; ?></td>
            </tr>
			<tr height="40px">
              <td width="180">State</td>
              <td colspan="2" class="highlight"><?php echo $rows->p_state; ?></td>
            </tr>
			<tr height="40px">
              <td width="180">Phone Number</td>
              <td colspan="2" class="highlight"><?php echo $rows->p_phone; ?></td>
            </tr>
			<tr height="40px">
              <td width="180">Mobile Number</td>
              <td colspan="2" class="highlight"><?php echo $rows->p_mobile; ?></td>
            </tr>
			<tr height="40px">
              <td width="180">Email Address</td>
              <td colspan="2" class="highlight"><?php echo $rows->p_email; ?></td>
            </tr>
			<tr height="40px">
              <td width="180">Chip No</td>
              <td colspan="2" class="highlight"><?php echo $rows->p_chipno; ?></td>
            </tr>			
          </table>
		</div>  		
	</div>  
	<div Style="width:100%;clear:both;"> 
		<p>This case sheet is hospital property . Not valid for vetero-legal purposes</p>
	</div>
				<table style="border-collapse: collapse;width: 100%;border:1px solid black;">
					<tr>
						<th style="border: 1px solid black;text-align:center;border:1px solid black;height:40px;">Date</th>
						<th style="border: 1px solid black;text-align:center;border:1px solid black;height:40px;">Amount</th>
						<th style="border: 1px solid black;text-align:center;border:1px solid black;height:40px;">Receipt No:</th>
						<th style="border: 1px solid black;text-align:center;border:1px solid black;height:40px;">Date</th>
						<th style="border: 1px solid black;text-align:center;border:1px solid black;height:40px;">Amount</th>
						<th style="border: 1px solid black;text-align:center;border:1px solid black;height:40px;">Receipt No:</th>
						<th style="border: 1px solid black;text-align:center;border:1px solid black;height:40px;">Valid Upto:</th>
					</tr>
					<tr>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
					</tr>
					<tr>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
					</tr>
					<tr>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
					</tr>
					<tr>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
						<td style="height:40px;border:1px solid black;"></td>
					</tr>
				
					
					
					
				</table>		  
<script language="javascript" type="text/javascript">
		window.onload = function() {
        document.body.offsetHeight;
       var is_chrome = function () { return Boolean(window.chrome); }
		if(is_chrome) 
		{
		   window.print();
		   setTimeout(function(){window.close();}, 10000); 
		   //give them 10 seconds to print, then close
		}
		else
		{
		   window.print();
		   window.close();
		}
        }
</script>
</body>
</html>
