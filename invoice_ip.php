<?php
	ob_start();
	session_start();
	include_once("./includes/connection.php");
	include_once("./includes/functions.php");
	include_once "loginchk.inc.php";
	$role = $_SESSION['a_role'];





	?>
<!DOCTYPE html>
<html>
<head>
<?php include_once('header.php'); ?>
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="plugins/datepicker/datepicker3.css">

<!-- <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css"> -->
 <link rel="stylesheet" href="plugins/select2/select2.min.css">
 <link rel="stylesheet" href="plugins/iCheck/all.css">
 <link  rel="stylesheet"  href="dist/css/styleinvoice.css">

<!-- date picker -->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <?php include_once('topbar.php'); ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('sidebar.php'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <h1>Invoice</h1> -->
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Invoice</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> IP Billing
            <small class="pull-right">Date: 2/10/2014</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">

                <label>Bill NO</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">1001</option>
                  <option>1002</option>
                  <option>1003</option>
                  <option>1004</option>

                </select>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
              <label>Patient Name</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">PET1001</option>
                  <option>PET1002</option>
                  <option>PET1003</option>
                  <option>PET1004</option>

                </select>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
              <label>Patient ID</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">PET1001</option>
                  <option>PET1002</option>
                  <option>PET1003</option>
                  <option>PET1004</option>

                </select>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
              <label>Date:</label>

              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input name="txtfdov" type="text" class="form-control pull-right" id="txtfdov" placeholder="date" required>

              </div>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class=" invoicelist-body">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped" id="tableId">
            <thead>
            <tr class="taxrelated">
              <th class="w10pr ">Sl.NO</th>
              <th class="w10pr ">Item</th>
              <th  class="w10pr "> Quantity</th>
              <th class="w10pr ">UnitRate</th>
              <th class="w10pr ">Amount</th>
              <th class="w10pr "></th>
            </tr>
            </thead>
            <tbody>
<tr>
                              <?php
                                  $slno = 1;?>
                      <td style="width:15px" class="count" id="rowid_0" >

                      <a class="control removeRow" href="#">x</a></td>
                      <input type="hidden" name="rowid" id="rowid"   />
                      <!-- <input type="hidden" list="medicine" name="hidden"/> -->
                      <?php


                      ?>


                      <datalist id="medicine">

                          <option value="" />


                          <option value=""/>
                      </datalist>
                      <td>
                          <input type="text" list="Item" name="mname" value="" class="medname select2 inputtext" id="medname_0"  placeholder="Item Name" autocomplete="off"  onchange="medicineCleanUp(this.id, this.value)" required=""/>
                      </td>
                      <!--                                            <td  ></td>     --><input type="hidden" name="batch" id="batch_0"   />
                      <td class="amount" >

                          <input type="number" style="width:50%" name="qty" id="qty_0" class="inputtext" value="" autocomplete="off" required=""  /></td>


                      <!-- <td class="taxrelated" id="qunty_0">
                      </td> -->

                      <td class="taxrelated" id="totalqunty_0"></td>

                      <td class="taxrelated sum" id="amt_0" name="value">

                      </td>

                      <td class="rate" > <input type="hidden"  name="rate" id="rate_0" /> </td>

                      </tr>


            </tbody>
          </table>

        </div>
        <a class="control newRow" href="#">+</a>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">

          <p class="lead">Payment Methods:</p>
          <div class="col-xs-3">
          <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Cash</option>
                  <option>Card</option>
                  <option>Check</option>


                </select>
                </div>
          <img src="../../dist/img/credit/visa.png" alt="Visa">
          <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
          <img src="../../dist/img/credit/american-express.png" alt="American Express">
          <img src="../../dist/img/credit/paypal2.png" alt="Paypal">

          <!-- <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
          </p> -->
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Amount Due 2/22/2014</p>

          <div class="table-responsive">
            <table class="">
              <tr>
                <th style="width:50%">Billed Amount:</th>
                <td><div class="col-xs-6">
                  <input type="text" class="form-control" >
                </div></td>
              </tr>
              <tr>
                <th>Paid Amount</th>
                <td><div class="col-xs-6">
                  <input type="text" class="form-control" id="amount_paid" name="amount_paid" >
                </div></td>
              </tr>
              <tr>
                <th>Balance</th>
                <td><div class="col-xs-6">
                  <input type="text" class="form-control" id="balance" name="balance" >
                </div></td>
              </tr>
              <tr>
                <th>Closed</th>
                <td> <div class="form-group  col-xs-6"><label>
                  <input type="checkbox" name="close" class="flat-red" checked>
                </label></div></td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div>
    </section>

  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include_once('footer.php');?>
  <div class="control-sidebar-bg"></div>
</div>

<!-- <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script> -->
<script src="dist/js/lib/jquery.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/select2.full.min.js"></script>
<script src="plugins/input-mask/jquery.inputmask.js"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>



<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="dist/js/lib/invoice-edit-ip.js"></script>

<script>
var text_max = 200;
$('#smscount_message').html(text_max + ' remaining');

$('#smsmessage').keyup(function() {
  var text_length = $('#smsmessage').val().length;
  var text_remaining = text_max - text_length;
  $('#smscount_message').html(text_remaining + ' remaining');
});
$(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
    $('#txtfdov').datepicker();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    // $('#reservation').daterangepicker();
    //Date range picker with time picker
    // $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    // $('#daterange-btn').daterangepicker(
    //     {
    //       ranges: {
    //         'Today': [moment(), moment()],
    //         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    //         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    //         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    //         'This Month': [moment().startOf('month'), moment().endOf('month')],
    //         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    //       },
    //       startDate: moment().subtract(29, 'days'),
    //       endDate: moment()
    //     },
    //     function (start, end) {
    //       $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    //     }
    // );

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    // $(".my-colorpicker1").colorpicker();
    //color picker with addon
    // $(".my-colorpicker2").colorpicker();

    //Timepicker
    // $(".timepicker").timepicker({
    //   showInputs: false
    // });
  });
  function itemDetailsfetch(id, value)
                {

                    var med_exp_batch_qty = value.split(" -> ");
                    var tid = id.split("medname_");

                    var med_id = "medname_" + tid[1];
                    var qty_id = "qty_" + tid[1];
                    var batch_id = "batch_" + tid[1];
                    var row_id = "rowid_" + tid[1];
                    var amount_id = "amt_" + tid[1];
                    var qtyhand_id = "qunty_" + tid[1];
                    var totalqty_id = "totalqunty_" + tid[1];
                    var batch_id = "batch_" + tid[1];

                    var rate_id = "rate_" + tid[1];

                    document.getElementById(id).value = med_exp_batch_qty[0];
                    document.getElementById(batch_id).value = med_exp_batch_qty[2];


                    var med = document.getElementById(med_id).value;
                    var qty = document.getElementById(qty_id).value; // value of single (first) medicine entered quantity
                    var batch = document.getElementById(batch_id).value;
                    var rid = document.getElementById(row_id).id;
                    var amount = document.getElementById(amount_id).innerHTML;
                    var totalqty = document.getElementById(totalqty_id).innerHTML;
                    var url = "pharmacyqtyrate?medname=" + med + "&batch=" + batch;


                    var rowindex = searchRidDetls(rid, med, batch, array);
                    if (rowindex != -1)
                    {

//                        var row = "rowid_" + rowindex;
                        var qt_id = "qty_" + rowindex;
//                        var m_id = "medname_" + rowindex;
                        document.getElementById(qty_id).value = "";
                        var medn = array[rowindex].med_name;
                        var batch1 = array[rowindex].batch;
//                         alert("hello   " + array[rowindex].qty);
                        array[rowindex].qty = 0;
//                        alert("hello   " + array[rowindex].qty);
                        var mIndex = searchMedDetls(rid, medn, batch1, array);


                        var newbatchqty = 0;
                        var q = 0;

                        for (var i = 0; i < mIndex.length; i++)
                        {
                            var q_id = array[mIndex[i]].qty;


                            q = Number(q) + Number(q_id);

//
                        }


                        for (var i = 0; i < mIndex.length; i++)
                        {
                            var roid = array[mIndex[i]].rowid;

                            var Tid = roid.split("rowid_");



                            var Qunty_id = "qunty_" + Tid[1];


                            newbatchqty = Number(array[Tid[1]].taqtyTrack) - Number(q);
                            array[mIndex[i]].taqty = newbatchqty;
                            document.getElementById(Qunty_id).innerHTML = newbatchqty;

                        }

                        $.post(url,
                                function (data, status)
                                {
                                    var obj = JSON.parse(data);

                                    var resultIndex = searchMedDetls(rid, med, batch, array);


                                    if (resultIndex === 0)
                                    {



                                        array[rowindex].rowid = rid;
                                        array[rowindex].med_name = med;
                                        array[rowindex].batch = batch;
                                        array[rowindex].qty = qty;
                                        array[rowindex].rate = obj[0].rate;
                                        array[rowindex].taqty = obj[0].quantity;
                                        array[rowindex].taqtyTrack = obj[0].quantity;
                                        array[rowindex].totalqty = obj[0].totalquantity;


//
                                        document.getElementById(qtyhand_id).innerHTML = obj[0].quantity;
                                        document.getElementById(totalqty_id).innerHTML = obj[0].totalquantity;
                                        document.getElementById(rate_id).value = obj[0].rate;

                                    } else
                                    {


                                        var newbatchqty = 0;
                                        var q = 0;

                                        for (var i = 0; i < resultIndex.length; i++)
                                        {
                                            var q_id = array[resultIndex[i]].qty;


                                            q = Number(q) + Number(q_id);

//
                                        }


                                        for (var i = 0; i < resultIndex.length; i++)
                                        {
                                            var roid = array[resultIndex[i]].rowid;

                                            var Tid = roid.split("rowid_");



                                            var Qunty_id = "qunty_" + Tid[1];


                                            newbatchqty = Number(array[Tid[1]].taqtyTrack) - Number(q);
                                            array[resultIndex[i]].taqty = newbatchqty
                                            document.getElementById(Qunty_id).innerHTML = newbatchqty;
                                        }

                                        array[rowindex].rowid = rid;
                                        array[rowindex].med_name = med;
                                        array[rowindex].batch = batch;
                                        array[rowindex].qty = qty;
                                        array[rowindex].rate = obj[0].rate;
                                        array[rowindex].taqty = obj[0].quantity;
                                        array[rowindex].taqtyTrack = obj[0].quantity;
                                        array[rowindex].totalqty = obj[0].totalquantity;

                                        document.getElementById(qtyhand_id).innerHTML = newbatchqty;
                                        document.getElementById(totalqty_id).innerHTML = obj[0].totalquantity;
                                        document.getElementById(rate_id).value = obj[0].rate;



                                    }

                                });
                    } else
                    {
                        $.post(url,
                                function (data, status)
                                {
                                    var obj = JSON.parse(data);
                                    var resultIndex = searchMedDetls(rid, med, batch, array);


                                    if (resultIndex === 0)
                                    {


                                        var object = {};
                                        object.rowid = rid;
                                        object.med_name = med;
                                        object.batch = batch;
                                        object.qty = qty;
                                        object.rate = obj[0].rate;
                                        object.taqty = obj[0].quantity;
                                        object.taqtyTrack = obj[0].quantity;
                                        object.totalqty = obj[0].totalquantity;


                                        array.push(object);
//
                                        document.getElementById(qtyhand_id).innerHTML = obj[0].quantity;
                                        document.getElementById(totalqty_id).innerHTML = obj[0].totalquantity;
                                        document.getElementById(rate_id).value = obj[0].rate;

                                    } else
                                    {


                                        var newbatchqty = 0;
                                        var q = 0;

                                        for (var i = 0; i < resultIndex.length; i++)
                                        {
                                            var q_id = array[resultIndex[i]].qty;


                                            q = Number(q) + Number(q_id);

//
                                        }


                                        for (var i = 0; i < resultIndex.length; i++)
                                        {
                                            var roid = array[resultIndex[i]].rowid;

                                            var Tid = roid.split("rowid_");



                                            var Qunty_id = "qunty_" + Tid[1];


                                            newbatchqty = Number(array[Tid[1]].taqtyTrack) - Number(q);
                                            array[resultIndex[i]].taqty = newbatchqty
                                            document.getElementById(Qunty_id).innerHTML = newbatchqty;
                                        }




                                        var object = {};
                                        object.rowid = rid;
                                        object.med_name = med;
                                        object.batch = batch;
                                        object.qty = qty;
                                        object.rate = obj[0].rate;
                                        object.taqty = newbatchqty;
                                        object.taqtyTrack = obj[0].quantity;
                                        object.totalqty = obj[0].totalquantity;

                                        array.push(object);

                                        document.getElementById(qtyhand_id).innerHTML = newbatchqty;
                                        document.getElementById(totalqty_id).innerHTML = obj[0].totalquantity;
                                        document.getElementById(rate_id).value = obj[0].rate;


                                    }

                                });
                    }

                }
                function removefun(id)
                {

                    var rid = id.attr('id');
                    var tid = rid.split("rowid_");



                    var medrem, batchrem, Qtyonbach;
                    var main_id = tid[1];
                    var pop_pos = 0; //pop position
                    for (var i = 0; i < array.length; i++)
                    {
                        if (rid == array[i].rowid)
                        {
                            medrem = array[i].med_name;
                            batchrem = array[i].batch;
                            pop_pos = i;

                        }
                    }
                    array.splice(pop_pos, 1);
                    var resultIndex = searchMedDetls(rid, medrem, batchrem, array);

                    var quantity = 0;

                    for (var i = 0; i < resultIndex.length; i++)
                    {



                        quantity = Number(quantity) + Number(array[resultIndex[i]].qty);

                    }


                    for (var i = 0; i < resultIndex.length; i++)
                    {
                        var roid = array[resultIndex[i]].rowid;

                        var Tid = roid.split("rowid_");



                        var Qunty_id = "qunty_" + Tid[1];


                        var qtytrack = Number(array[Tid[1]].taqtyTrack);
                        var avalqty = Number(qtytrack) - Number(quantity);


                        document.getElementById(Qunty_id).innerHTML = avalqty;
                    }

                }

</script>
</body>
</html>
