<?php
	ob_start();
	session_start();
	include_once("./includes/connection.php");
	include_once("./includes/functions.php");
	include_once "loginchk.inc.php";
	$a_name	= $_SESSION['a_name']; 
	$a_id	= $_SESSION['a_id']; 
	
	array_filter($_GET, 'trim_value');
		$postfilter =array(
				'pid'     		=> array('filter' => FILTER_SANITIZE_NUMBER_INT, 'flags' => ''),
		);
			$revised_post_array = filter_var_array($_GET, $postfilter);  
			$pid 				= $revised_post_array['pid'];
			
		$qrry=$mysqli->query("select p_name, p_pid from p_patient where p_id=$pid");
		$rows = mysqli_fetch_object($qrry);

	if(isset($_POST['subAdd']))
	{	
		$adtime = time();
		$qry_vaccine="INSERT INTO p_vaccination(p_id,v_date) VALUES('$pid','$adtime')"; 
		$mysqli->query($qry_vaccine) or die('Error, query failed');

		header ("location:manage_vaccination.php?pid=$pid");
	}
	?>
<!DOCTYPE html>
<html>
<head>
<script src="includes/jquery-1.9.1.min.js" type="text/javascript"></script>
<?php include_once('header.php'); ?>
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <?php include_once('topbar.php'); ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('sidebar.php'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Manage Vaccination</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Manage Vaccination</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage Vaccination</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<h4 class="box-title">Patient ID:  <?php echo $rows->p_pid; ?></h4>
				<h4 class="box-title">Name:  <?php echo $rows->p_name; ?></h4>
				<h3 class="box-title">Date of Vaccination</h3>				
                <?php 
					$qry=$mysqli->query("SELECT * FROM p_vaccination WHERE p_id='$pid'  ORDER BY v_id ASC");
					$count = mysqli_num_rows($qry);$i=1;
					while($cin = mysqli_fetch_array($qry)){
					?>
					<div class="col-sm-1 col-md-1" style="background: #f2f2f2;margin: 2px;">
					<p align="center"><?php echo $i;  ?></p>
					<p align="center">
					<a style="color: green;"><i class="fa fa-calendar-check-o" style="font-size: 27px;"></i></a></p>
					<p align="center"><?php echo date('d-m-Y',$cin['v_date']) ?></p>
					
					</div>
                  
                <?php  $i++;} ?>
					<div class="col-sm-1 col-md-1" style="background: #e3e3e3;margin: 2px;">
					<a href="add_vaccination.php?pid=<?php echo $pid ?>" title="Make it Active" style="color: red;"><p align="center">&nbsp</p>
					<p align="center"><i class="fa fa-calendar-plus-o" style="font-size: 27px;"></i></p>
					<p align="center">Add</p></a>
					</div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include_once('footer.php');?>
  <div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#txtdov').datepicker();
	
  });
   $(document).ready(function() {
	$('a[data-confirm]').click(function(ev) {
		var href = $(this).attr('href');
		if (!$('#dataConfirmModal').length) {
			$('body').append('<div id="dataConfirmModal" class="modal" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button><h3 id="dataConfirmLabel">Please Confirm</h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button><a class="btn btn-primary" id="dataConfirmOK">OK</a></div></div>');
		} 
		$('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
		$('#dataConfirmOK').attr('href', href);
		$('#dataConfirmModal').modal({show:true});
		return false;
	});
});
</script>
</body>
</html>
