<?php
	ob_start();
	session_start();
	include_once("./includes/connection.php");
	include_once("./includes/functions.php");
		include_once "loginchk.inc.php";
		array_filter($_GET, 'trim_value');
		$postfilter =array(
				'id'     		=> array('filter' => FILTER_SANITIZE_NUMBER_INT, 'flags' => ''),
		);
			$revised_post_array = filter_var_array($_GET, $postfilter);  
			$id 				= $revised_post_array['id'];
			
		$qry=$mysqli->query("select * from p_patient where p_id=$id");
		$rows = mysqli_fetch_object($qry);
		
	?>
<!DOCTYPE html>
<html>
<head>
<script src="includes/jquery-1.9.1.min.js" type="text/javascript"></script>

<style>
    h1 {
        color: #444;
        font-family: helvetica;
        font-size: 20px;
		text-transform: uppercase;
    }
    table.first {
        color: #444;
        font-family: helvetica;
        border: 3px solid #fff;
        background-color: #fff;
    }
    td {
        border: 2px solid #fff;
        background-color: #fff;
    }
	.highlight{
		color: #222;
		font-weight:bold;
	}
	@media print 
{
  @page { margin-top: 0; }
  body  { margin-top: 1.6cm; }
}
}

</style>
<?php include_once('header.php'); ?>
<!-- date picker -->
<link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">

          <table class="first" cellpadding="6" cellspacing="4" border="0" nobr="true" width="700px">
            <tr><td colspan="4">
				<table class="first" border="0" width="100%" nobr="true" >
					<tr>
						<td width="210"><img width="120" align="left" src="images/logo.png" border="0"></td>
						<td align="left"> <h2 align="left">the cochin pet hospital</h2>
						<h2 align="left">CLINICAL CASE REPORT</h2><br>
						<br></td>
					</tr>
				</table>
			</td>
            </tr>
            <tr>
              <td width="140">Date of Joining</td>
              <td colspan="2" class="highlight"><?php echo date('d-m-Y',$rows->p_date); ?></td>
              <td width="164" rowspan="6"><?php if($rows->p_photo != ""){?>
                <img src="<?php echo "files/photos/".$rows->p_photo; ?>" height="150" width="160" > <br />
                <?php }  else{ ?>
                <img src="images/noimage.png" width="150" height="160" border="0">
              <?php } ?></td>
            </tr>
            <tr>
              <td>Patient ID</td>
              <td colspan="2" class="highlight"><?php echo $rows->p_pid; ?></td>
            </tr>
            <tr>
              <td>Registration Fees</td>
              <td colspan="2" class="highlight"><?php echo $rows->p_regfees; ?></td>
            </tr>
            <tr>
              <td>Gender</td>
              <td colspan="2" class="highlight"><?php echo $rows->p_gender; ?></td>
            </tr>
            <tr>
              <td>Species</td>
              <td colspan="2" class="highlight"><?php $asso = $mysqli->query("SELECT c_name FROM p_category where c_id =".$rows->p_species);
						 $ass = $asso->fetch_object();
						 echo $species = $ass->c_name;?></td>
            </tr>
            <tr height="30px">
              <td>Animal</td>
              <td colspan="2" class="highlight"><?php $assa = $mysqli->query("SELECT c_name FROM p_category where c_id =".$rows->p_animal);
						 $assa = $assa->fetch_object();
						echo $animal = $assa->c_name;?></td>
            </tr>
            <tr height="30px">
              <td>Breed</td>
              <td colspan="3" class="highlight"><?php $assb = $mysqli->query("SELECT c_name FROM p_category where c_id =".$rows->p_breed);
						 $assb = $assb->fetch_object();
						 echo $breed = $assb->c_name;?></td>
            </tr>
            <tr height="30px">
              <td>Name</td>
              <td colspan="3" class="highlight"><?php echo $rows->p_name;?></td>
            </tr>
            <tr  height="30px">
              <td>DOB & AGE</td>
              <td colspan="3" class="highlight"><?php if($rows->p_dob != ''){
				   	$age =_date_diff($rows->p_dob, time());
					$days =  $age[days];
					$years = ($days / 365) ; 
					$years = floor($years); 
					$month = ($days % 365) / 30.5; 
					$month = floor($month); 
					$cage =  $years." year(s), ".$month." month(s)";
					} 
					 echo 	$dobirth =  date('d-m-Y',$rows->p_dob);
					echo ", ".$cage;
			?>
		</td>
            </tr>
			
			
			
			
			
            <tr  height="30px">
              <td>Type of Vaccination</td>
              <td width="120" class="highlight"><?php echo $rows->p_puppytype;?></td>
			  <td width="110">Owner Name</td>
              <td class="highlight"><?php echo $rows->p_ownername;?></td>
            </tr>
            <tr  height="30px">
              <td>Date of First Vaccination</td>
              <td class="highlight"><?php echo date('d-m-Y',$rows->p_firstvaccination); ?></td>
			  <td>Address</td>
              <td class="highlight"><?php echo $rows->p_address; ?></td>
            </tr>
            <tr  height="30px">
              <td>Colour</td>
              <td class="highlight"><?php echo $rows->p_color; ?></td>
			   <td>Pincode</td>
              <td class="highlight"><?php echo $rows->p_pincode; ?></td>
            </tr>
            <tr  height="30px">
              <td>Weight</td>
              <td class="highlight"><?php echo $rows->p_wieght;?></td>
			  <td>State</td>
              <td class="highlight"><?php echo $rows->p_state; ?></td>
            </tr>
            <tr  height="30px">
              <td>Chip No</td>
              <td class="highlight"><?php echo $rows->p_chipno;?> </td>
			  <td>Phone Number</td>
              <td class="highlight"><?php echo $rows->p_phone; ?></td>
            </tr>
            <tr  height="30px">
              <td>Health Card No</td>
              <td class="highlight"><?php echo $rows->p_healthcard; ?> </td>
			  <td>Mobile Number</td>
              <td class="highlight"><?php echo $rows->p_mobile; ?></td>
            </tr>
            <tr  height="30px">
              <td>Valid</td>
              <td class="highlight"><?php echo $rows->p_valid?></td>
			   <td>Email address</td>
              <td class="highlight"><?php echo $rows->p_email; ?></td>
            </tr>
			<tr  height="30px">
              <td colspan="4">This case sheet is hospital property . Not valid for vetero-legal purposes</td>
            </tr>
			
	
          </table>
<script language="javascript" type="text/javascript">
		window.onload = function() {
        document.body.offsetHeight;
       var is_chrome = function () { return Boolean(window.chrome); }
		if(is_chrome) 
		{
		   window.print();
		   setTimeout(function(){window.close();}, 10000); 
		   //give them 10 seconds to print, then close
		}
		else
		{
		   window.print();
		   window.close();
		}
        }
</script>
</body>
</html>
