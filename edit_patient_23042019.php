<?php
/* ini_set('display_errors', 'Off');  */ 
	ob_start();
	session_start();
	include_once("./includes/connection.php");
	include_once("./includes/functions.php");
		include_once "loginchk.inc.php";
		array_filter($_GET, 'trim_value');
		$postfilter =array(
				'id'     		=> array('filter' => FILTER_SANITIZE_NUMBER_INT, 'flags' => ''),
		);
			$revised_post_array = filter_var_array($_GET, $postfilter);  
			$id 				= $revised_post_array['id'];
			
		$qry=$mysqli->query("select * from p_patient where p_id=$id");
		$rows = mysqli_fetch_object($qry);
		/* print_r($rows);exit; */
		$species_array = explode(',',$rows->p_vaccination);		 
		$p_image1 = $rows->u_photo;
		
		if(isset($_POST['subAdd']))
		{	
			
			if($_POST['removesmallimage1'] == 'remove'){
					$do1=unlink("files/photos/$p_image1");
					$qry11=$mysqli->query("update p_patient  set u_photo = '' where p_id ='$id'");
			}
			if($_FILES['photo']['name'] != '') 
			{ 
				$updImg1     =    "files/temp/"; 
				$fileImg1    =    $_FILES['photo']['name']; 
				$updImg1     =    $updImg1.$fileImg1; 
				$ImgUpload1  =    move_uploaded_file($_FILES['photo']['tmp_name'],$updImg1); 
				$photo=resizeimage($updImg1,"files/photos/",$fileImg1,250); 
				$qry1=$mysqli->query("update p_patient  set u_photo = '$photo' where p_id ='$id'");
			}
		
		$adtime = time();
		array_filter($_POST, 'trim_value');
		$postfilter =array(
			
			'txtpatientid'    	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtregfees'    	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'rgender'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'selspecies'   		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'selanimal'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'selbreed'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtname'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'selbday'     		=> array('filter' => FILTER_SANITIZE_NUMBER_INT, 'flags' => ''),
			'selbmonth'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'selbyear'     		=> array('filter' => FILTER_SANITIZE_NUMBER_INT, 'flags' => ''),
			'totreatment'     => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'tovaccination'     => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			
			
			'puppy'     => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'dog'     => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'kitten'     => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'cat'     => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'smallimagehidden1'     => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			
			
			'txtcolour'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtweight'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtchipno'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txthcardno'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'valid'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtowner'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtaddress'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtpincode'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtstate'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtphone'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtmobile'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtemail'     		=> array('filter' => FILTER_SANITIZE_EMAIL, 'flags' => ''),
		
		);
		$revised_post_array = filter_var_array($_POST, $postfilter);  
		$revised_post_array = sanitizearray($revised_post_array, $mysqli);
		if($revised_post_array['puppy']){
			$vaccination=$revised_post_array['tovaccination'].",".$revised_post_array['puppy'];;
		}
		if($revised_post_array['kitten']){
			$vaccination=$revised_post_array['tovaccination'].",".$revised_post_array['kitten'];
		}
		
		if($_FILES['photo']['name'] != '') 
				{ 
					$updImg1     =    "files/temp/"; 
					$fileImg1    =    $_FILES['photo']['name']; 
					$updImg1     =    $updImg1.$fileImg1; 
					$ImgUpload1  =    move_uploaded_file($_FILES['photo']['tmp_name'],$updImg1); 
					$photo=resizeimage($updImg1,"files/photos/",$fileImg1,250); 
		}else{ 
			$photo=""; 
		}
		//BK
/*  		 print_r($revised_post_array['smallimagehidden1']);
		 exit;  */
		 
		$doj = strtotime(str_replace('/', '-',$_POST['txtdoj']));
		$dob = strtotime($_POST['selbday'].'-'.$_POST['selbmonth'].'-'.$_POST['selbyear']) ; 
		
		$qry_user="UPDATE  p_patient SET p_date='$doj',p_pid='".$revised_post_array['txtpatientid']."',p_regfees='".$revised_post_array['txtregfees']."',p_gender='".$revised_post_array['rgender']."',p_name='".$revised_post_array['txtname']."',p_species='".$revised_post_array['selspecies']."',p_animal='".$revised_post_array['selanimal']."',p_breed='".$revised_post_array['selbreed']."',p_dob='$dob',p_vaccination='$vaccination',p_treatment='".$revised_post_array['totreatment']."',p_color='".$revised_post_array['txtcolour']."',p_wieght='".$revised_post_array['txtweight']."',p_chipno='".$revised_post_array['txtchipno']."',p_healthcard='".$revised_post_array['txthcardno']."',p_valid='".$revised_post_array['valid']."',p_ownername='".$revised_post_array['txtowner']."',p_address='".$revised_post_array['txtaddress']."',p_pincode='".$revised_post_array['txtpincode']."',p_state='".$revised_post_array['txtstate']."',p_phone='".$revised_post_array['txtphone']."',p_mobile='".$revised_post_array['txtmobile']."',p_email='".$revised_post_array['txtemail']."',p_photo='".$revised_post_array['smallimagehidden1']."' WHERE p_id = '$id' "; 
		$mysqli->query($qry_user) or die('Error, query failed');
		header ("location:manage_patient.php");
		
		}	
	?>
<!DOCTYPE html>
<html>
<head>
<script src="includes/jquery-1.9.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
$('.ageclass').change(function(){
var dat = new Date();
var curday = dat.getDate();
var curmon = dat.getMonth()+1;
var curyear = dat.getFullYear();
var calday = document.getElementById("selbday").value;
var calmon = document.getElementById("selbmonth").value;
var calyear = document.getElementById("selbyear").value;
	if(curday == "" || curmon=="" || curyear=="" || calday=="" || calmon=="" || calyear=="")
	{
		return false;
	}	
	else
	{
		var curd = new Date(curyear,curmon-1,curday);
		var cald = new Date(calyear,calmon-1,calday);
		var diff =  Date.UTC(curyear,curmon,curday,0,0,0) - Date.UTC(calyear,calmon,calday,0,0,0);
		var dife = datediff(curd,cald);
		var agevalue1=dife[0];
		var agevalue2=dife[1];
		if(agevalue1==0){
			var agevalue=dife[1]+" months";
		}else {
			var agevalue=dife[0]+" year(s) "+ dife[1]+" month(s)";;
		}
	}
        $('#age').val(agevalue);
    });
});
function checkleapyear(datea)
{
	if(datea.getYear()%4 == 0)
	{
		if(datea.getYear()% 10 != 0)
		{
			return true;
		}
		else
		{
			if(datea.getYear()% 400 == 0)
				return true;
			else
				return false;
		}
	}
  return false;
}
function DaysInMonth(Y, M) {
    with (new Date(Y, M, 1, 12)) {
        setDate(0);
        return getDate();
    }
}
function datediff(date1, date2) {
    var y1 = date1.getFullYear(), m1 = date1.getMonth(), d1 = date1.getDate(),
	 y2 = date2.getFullYear(), m2 = date2.getMonth(), d2 = date2.getDate();
    if (d1 < d2) {
        m1--;
        d1 += DaysInMonth(y2, m2);
    }
    if (m1 < m2) {
        y1--;
        m1 += 12;
    }
    return [y1 - y2, m1 - m2, d1 - d2];
}
</script>
<?php include_once('header.php'); ?>
<!-- date picker -->
<link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <!-- Main Header -->
  <?php include_once('topbar.php'); ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('sidebar.php'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Patient</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Edit Patient</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Patient</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form class="form-horizontal" action=""  method="post" name="frm1" enctype="multipart/form-data">
              <div class="box-body">
			  
				<div class="form-group">
                  <label class="col-sm-4 control-label">Registration Date</label>
				   <div class="col-sm-8">
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input  name="txtdoj" type="text" class="form-control pull-right" id="txtdoj" placeholder="Registration Date" value="<?php echo date('d-m-Y',$rows->p_date); ?>">
                </div>
				  </div>
                </div>
              <div class="form-group">
                  <label class="col-sm-4 control-label">Patient ID</label>
				   <div class="col-sm-8">
                  <input type="text" name="txtpatientid" class="form-control"  placeholder="Enter Patient ID" value="<?php echo $rows->p_pid; ?>">
				  </div>
           </div>
		   <div class="form-group">
                  <label class="col-sm-4 control-label">Registration Fees</label>
				   <div class="col-sm-8">
                  <input type="text" name="txtregfees" class="form-control"  placeholder="Enter Registration Fees" value="<?php echo $rows->p_regfees; ?>">
				  </div>
           </div>
		   
			    <div class="form-group">
                  <label class="col-sm-4 control-label">Gender</label>
				   <div class="col-sm-8">
                  
                      <input type="radio" name="rgender" id="rgender" value="Male" <?php if(!(strcmp($rows->p_gender,"Male"))) {echo "checked=\"checked\"";} ?> > Male
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    
                      <input type="radio" name="rgender" id="rgender" value="Female" <?php if(!(strcmp($rows->p_gender,"Female"))) {echo "checked=\"checked\"";} ?> > Female
              
				  </div>
                </div>
				
				  <div class="form-group">
                  <label class="col-sm-4 control-label">Species</label>
				   <div class="col-sm-8">
				   <select name="selspecies" class="form-control" id="species"  onChange="selectCategory(this.options[this.selectedIndex].value)" >
              <option value="" >Select Species</option>
			     <?php
                   $codemqry = "SELECT * FROM  p_category WHERE c_status ='1' AND c_parent = '0'  ORDER BY c_id ASC ";  
                    $codemres = $mysqli->query($codemqry);
                    while($codobj = $codemres->fetch_array())
					 {
									   ?>
                    <option <?php if($codobj['c_id'] == $rows->p_species ){ echo "selected" ; } ?>  value="<?php echo $codobj['c_id'] ?>"><?php echo $codobj['c_name'] ?></option>
                    <?php 	} ?>
            </select>
				  </div>
                </div>
				  <div class="form-group">
                  <label class="col-sm-4 control-label">Animal</label>
				   <div class="col-sm-8">
				   <select id="category_dropdown" name="selanimal" class="form-control" onChange="selectSubCategory(this.options[this.selectedIndex].value)">
                     <option value="" >Select Animal</option>
					        <?php
					  if($rows->p_animal != ''){
						$csql = $mysqli->query("SELECT c_id,c_name FROM p_category WHERE c_status ='1' AND c_parent =".$rows->p_species);
					}else { 
						$csql = $mysqli->query("SELECT c_id,c_name FROM p_category WHERE c_status ='1' ORDER BY c_mid ASC");
					}
                    while($codc = $csql->fetch_array())
					 {
					 ?>
                    <option <?php if($codc['c_id'] == $rows->p_animal ){ echo "selected" ; } ?> value="<?php echo $codc['c_id'] ?>"><?php echo $codc['c_name'] ?></option>
                    <?php 	} ?>
					 </select>
					 
					 </div>
					 </div>
                     <div class="form-group">
                     <label class="col-sm-4 control-label">Breed</label>
                     <div class="col-sm-8">
				   <select id="subcategory_dropdown" name="selbreed" class="form-control">
                     <option value="" >Select Breed</option>
					    <?php
					  if($rows->p_breed != ''){
						$scsql = $mysqli->query("SELECT c_id,c_name FROM p_category WHERE c_status ='1'  AND c_parent=".$rows->p_animal);
					}else { 
						$scsql = $mysqli->query("SELECT c_id,c_name FROM p_category WHERE c_status ='1'  ORDER BY c_id ASC");
					}
                    while($scodc = $scsql->fetch_array())
					 {
					 ?>
                    <option <?php if($scodc['c_id'] == $rows->p_breed ){ echo "selected" ; } ?> value="<?php echo $scodc['c_id'] ?>"><?php echo $scodc['c_name'] ?></option>
                    <?php 	} ?>
					 </select>
					 
					 </div>
                     </div>
					 
                     <div class="form-group">
                     <label class="col-sm-4 control-label">
				     Name
				     </label>
                     <div class="col-sm-8">
                     <input type="text" name="txtname" class="form-control"  placeholder="Enter Name" value="<?php echo $rows->p_name; ?>">
                     </div>
                     </div>
					 
                     <?php 
			  if($rows->p_dob != ''){
			  $dobirth =  date('d-m-Y',$rows->p_dob); 
			  	$bday = substr($dobirth,0,2);
			  	$bmonth = substr($dobirth,3,2);
				$byear = substr($dobirth,6,4); }
			  ?>
              <div class="form-group">
                <label class="col-sm-4 control-label">Date of Birth</label>
                <div class="col-sm-8">
                  <select class="form-control col-sm-2 ageclass" style="width:150px;" name="selbday" id="selbday" >
                    <option value="">Day</option>
					  <?php for($i=1;$i<=31;$i++) { 
							if($i<10)
							$i	= "0".$i;
							$selected = '';
						 	if ($bday == $i) $selected = ' selected="selected"';
  								  print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
						 }	?>
                    <?php 
				for($i=1;$i<=31;$i++){
					if($i<10)
					$i	= "0".$i;
				?>
                    <option value="<?php echo $i?>"><?php echo $i;?></option>
                    <?php } ?>
                  </select>
                  <select class="form-control col-sm-2 ageclass" style="width:150px; margin-left:5px;" name="selbmonth" id="selbmonth">
                    <option value="">Month</option>
                    <option value="01" <?php if ($bmonth=='01') { ?>selected="selected" <?php } ?>>January</option>
                  <option value="02" <?php if ($bmonth=='02') { ?>selected="selected" <?php } ?>>February</option>
                  <option value="03" <?php if ($bmonth=='03') { ?>selected="selected" <?php } ?>>March</option>
                  <option value="04" <?php if ($bmonth=='04') { ?>selected="selected" <?php } ?>>April</option>
                  <option value="05" <?php if ($bmonth=='05') { ?>selected="selected" <?php } ?>>May</option>
                  <option value="06" <?php if ($bmonth=='06') { ?>selected="selected" <?php } ?>>June</option>
                  <option value="07" <?php if ($bmonth=='07') { ?>selected="selected" <?php } ?>>July</option>
                  <option value="08" <?php if ($bmonth=='08') { ?>selected="selected" <?php } ?>>August</option>
                  <option value="09" <?php if ($bmonth=='09') { ?>selected="selected" <?php } ?>>September</option>
                  <option value="10" <?php if ($bmonth=='10') { ?>selected="selected" <?php } ?>>October</option>
                  <option value="11" <?php if ($bmonth=='11') { ?>selected="selected" <?php } ?>>November</option>
                  <option value="12" <?php if ($bmonth=='12') { ?>selected="selected" <?php } ?>>December</option>
                  </select>
                  <select class="form-control col-sm-2 ageclass" style="width:150px; margin-left:5px;" name="selbyear" id="selbyear" >
                    <option value="">Year</option>
                
					   <?php 
					   $cdate = 2000;
						for($x=date('Y');$x>=$cdate;$x--){
					   
							$selected = '';
						 	if ($byear == $x) $selected = 'selected="selected"';
  								  print('<option value="'.$x.'"'.$selected.'>'.$x.'</option>'."\n");
						 }	?>
                  </select>
				   <?php if($rows->p_dob != ''){
				   	$age =_date_diff($rows->p_dob, time());
					$days =  $age[days];
					$years = ($days / 365) ; 
					$years = floor($years); 
					$month = ($days % 365) / 30.5; 
					$month = floor($month); 
					$cage =  $years." year(s), ".$month." month(s)";
					} ?>
                  <input type="text" id="age" value="<?php echo $cage ?>" name="txtage" class="form-control col-sm-2"  style="width:150px; margin-left:5px;" placeholder="Age" disabled="disabled">
                </div>
              </div>
			  

				
				<div class="form-group">
                    <label class="col-sm-4 control-label">Treatment Type *</label>
					<div class="col-sm-8">
						<div class="col-sm-4">
							<div>
								<input type="checkbox" name="totreatment" id="totreatment" value="treatment" <?php if($rows->p_treatment =="treatment"){echo "checked";} ?> >&nbsp;&nbsp;Treatment
							</div>
						</div>
						<div class="col-sm-4" id="vaccination_detail" >
							<div style="<?php if(in_array("vaccination", $species_array)){echo "display:block;";}else{echo "display:none;";} ?>">
								<input type="checkbox" name="tovaccination" id="tovaccination" value="vaccination" required onclick="openvaccination()"  <?php if(in_array("vaccination", $species_array)){echo "checked";} ?>>&nbsp;&nbsp;Vaccination
							</div>
							<div id="vaccination_check" style="<?php if(in_array('cat',$species_array) || in_array('kitten',$species_array)){echo "display:none;";} ?>">
								<input type="radio" name="puppy"  value="puppy" id="dog_vaccination"   <?php if(in_array("puppy", $species_array)){echo "checked";} ?><?php if(in_array('kitten','cat', $species_array)){echo "disabled";}?>> Puppy &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="radio" name="puppy"  value="dog" id="dog_vaccination1"  <?php if(in_array("dog", $species_array)){echo "checked";}if(in_array("kitten","cat", $species_array)){echo "disabled";}  ?>> Dog &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</div>
							<div id="vaccination_check1" style="<?php if(in_array('dog',$species_array) || in_array('puppy',$species_array)){echo "display:none;";}?>">
								<input type="radio" name="kitten"  value="kitten" id="cat_vaccination"  <?php if(in_array("kitten", $species_array)){echo "checked";}if(in_array("puppy","dog", $species_array)){echo "disabled";}  ?>> Kitten &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="radio" name="kitten"  value="cat" id="cat_vaccination1"  <?php if(in_array("cat", $species_array)){echo "checked";}if(in_array("puppy","dog", $species_array)){echo "disabled";}  ?>> Cat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</div>							
						</div>
					
					</div>
                </div>
				
				
				
				
				<div class="form-group">
                  <label class="col-sm-4 control-label">Date of First Vaccination</label>
				   <div class="col-sm-8">
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input disabled="disabled" name="txtfdov" type="text" class="form-control pull-right" id="txtfdov" placeholder="Date of First Vaccination"  value="<?php echo date('d-m-Y',$rows->p_firstvaccination); ?>">
                </div>
				  </div>
                </div>
			  
			  
				<div class="form-group">
                  <label class="col-sm-4 control-label">Colour</label>
				   <div class="col-sm-8">
                  <input name="txtcolour" type="text" class="form-control"  placeholder="Enter Colour" value="<?php echo $rows->p_color; ?>">
				  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Weight</label>
				   <div class="col-sm-8">
                  <input name="txtweight" type="text" class="form-control"  placeholder="Enter Weight" value="<?php echo $rows->p_wieght; ?>">
				  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Chip No</label>
				   <div class="col-sm-8">
                  <input name="txtchipno" type="text" class="form-control"  placeholder="Enter Chip No" value="<?php echo $rows->p_chipno; ?>">
				  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Health Card No</label>
				   <div class="col-sm-8">
                  <input name="txthcardno" type="text" class="form-control"  placeholder="Enter Health Card No" value="<?php echo $rows->p_healthcard; ?>">
				  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Valid</label>
				   <div class="col-sm-8">
                  
                      <input type="radio" name="valid" id="valid" value="Yes" <?php if(!(strcmp($rows->p_valid,"Yes"))) {echo "checked=\"checked\"";} ?> > Yes
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    
                      <input type="radio" name="valid" id="valid" value="No" <?php if(!(strcmp($rows->p_valid,"No"))) {echo "checked=\"checked\"";} ?> > No
              
				  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Owner Name</label>
				   <div class="col-sm-8">
                  <input name="txtowner" type="text" class="form-control" placeholder="Enter Name of Owner" value="<?php echo $rows->p_ownername; ?>">
				  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Address</label>
				   <div class="col-sm-8">
				  <textarea name="txtaddress" class="form-control "  rows="3" placeholder="Enter Address of Owner"><?php echo $rows->p_address; ?></textarea>
                </div>
		   </div>
				
					<div class="form-group">
                  <label class="col-sm-4 control-label">Pincode</label>
				   <div class="col-sm-8">
                  <input name="txtpincode" type="text" class="form-control" placeholder="Enter Pincode" value="<?php echo $rows->p_pincode; ?>">
				  </div>
                </div>


					<div class="form-group">
                  <label class="col-sm-4 control-label">State</label>
				   <div class="col-sm-8">
                  <input name="txtstate" type="text" class="form-control" placeholder="Enter State" value="<?php echo $rows->p_state; ?>">
				  </div>
                </div>

				  <div class="form-group">
                  <label class="col-sm-4 control-label">Phone Number</label>
				   <div class="col-sm-8">
                  <input type="text" name="txtphone" class="form-control  col-sm-4"   placeholder="Enter Phone Number" value="<?php echo $rows->p_phone; ?>">
				  </div>
                </div>
				
				  <div class="form-group">
                  <label class="col-sm-4 control-label">Mobile Number</label>
				   <div class="col-sm-8">
                  <input type="text" name="txtmobile" class="form-control  col-sm-4"  placeholder="Enter Mobile Number" value="<?php echo $rows->p_mobile; ?>">
				  </div>
                </div>
				
			
				<div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-4 control-label">Email address</label>
				   <div class="col-sm-8">
                  <input name="txtemail" type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Email address" value="<?php echo $rows->p_email; ?>">
				  </div>
                </div>
				
                 <?php if($rows->p_photo == ""){?>
              <div class="form-group">
                <label class="col-sm-4 control-label">Photo</label>
                <div class="col-sm-8">
                  <input type="file" id="exampleInputFile"  name="photo">
                </div>
              </div>
              <?php } else { ?>
              <div class="form-group">
                <label class="col-sm-4 control-label">Photo</label>
                <div class="col-sm-8">
                  <input type="hidden" name="smallimagehidden1" id="smallimagehidden1" value="<?php echo $rows->p_photo; ?>" />
                  <span class="style3">
                  <input name="removesmallimage1" type="checkbox" id="removesmallimage1" value="remove" onClick="if(this.checked) {document.getElementById('imagedis1').style.display=''} else {document.getElementById('imagedis1').style.display='none'}" />
                  <span class="style1">Remove</span></span> <img src="<?php echo "files/photos/".$rows->p_photo; ?>" height="100" width="100" > </div>
              </div>
              <div id="imagedis1" style="display:none">
                <div class="form-group" style="margin-left:330px;">
                  <table>
                    <tr>
                      <td width="305" height="43" align="left"><input type="file" id="exampleInputFile"  name="photo"></td>
                    </tr>
                  </table>
                </div>
              </div>
              <br>
              <?php } ?>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="subAdd" class="btn btn-primary">Submit</button>
              </div>
      </form>
        </div>
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
  <?php include_once('footer.php');?>
  <div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- date-range-picker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#txtdoj').datepicker();
	$('#txtfdov').datepicker();
	$('#example1').dataTable( {
  "columnDefs": [
		{ "width": "5%", "targets": [0,3,4,5,6,7] },
	]
	});
  });
</script>
<script type="text/javascript">

function selectCategory(c_id){
	//Bk-start
	if(c_id == 1){
		document.getElementById("vaccination_detail").style.display = "block";	
		document.getElementById("tovaccination").checked  = false;
		document.getElementById("vaccination_check").style.display = "none";	
		document.getElementById("vaccination_check1").style.display = "none";
	}
	if(c_id == 6){
		document.getElementById("vaccination_detail").style.display = "block";
		document.getElementById("tovaccination").checked  = false;		
		document.getElementById("vaccination_check").style.display = "none";	
		document.getElementById("vaccination_check1").style.display = "none";		
	

	}
	if(c_id == 52){
		document.getElementById("vaccination_detail").style.display = "none";
		document.getElementById("tovaccination").checked  = false;
		document.getElementById("vaccination_check").style.display = "none";	
		document.getElementById("vaccination_check1").style.display = "none";	
		document.getElementById("cat_vaccination").checked  = false;
		document.getElementById("cat_vaccination1").checked  = false;
		document.getElementById("dog_vaccination").checked  = false;
		document.getElementById("dog_vaccination1").checked  = false;		
	}

	//BK-end
	if(c_id!=""){
		loadData('category',c_id);
		//loadData('subcategory',c_id);
	}else{
		$("#category_dropdown").html("<option value=''>Select Animal</option>");
	}
}

function selectSubCategory(c_id){

	if(c_id!=""){
		loadData('subcategory',c_id);
	}else{
		$("#subcategory_dropdown").html("<option value=''>Select Breed</option>");		
	}
}

function loadData(loadType,loadId){
	var dataString = 'loadType='+ loadType +'&loadId='+ loadId;
	$("#"+loadType+"_loader").show();
    $("#"+loadType+"_loader").fadeIn(400).html('<img src="images/loader.gif" style="padding: 5px; margin-left: 5px;" />');
	$.ajax({
		type: "POST",
		url: "loadCategory.php",
		data: dataString,
		cache: false,
		success: function(result){
			$("#"+loadType+"_loader").hide();
			$("#"+loadType+"_dropdown").html(result);  
		}
	});
}
//Bk-start
function openvaccination() {
	//select box value
  var species = document.getElementById("species").value;	
  var checkBox = document.getElementById("tovaccination");
  var text = document.getElementById("vaccination_check");
  var text1 = document.getElementById("vaccination_check1");
  if (checkBox.checked == true){
	  if(species == 1){
			text.style.display = "block";
			document.getElementById("dog_vaccination").checked  = false;
			document.getElementById("dog_vaccination1").checked  = false;
			text1.style.display = "none";		
	  }
	  if(species == 6){
			text1.style.display = "block";
			document.getElementById("cat_vaccination").checked  = false;
			document.getElementById("cat_vaccination1").checked  = false;		
			text.style.display = "none"; 

	  }
  } else {
		text.style.display = "none";
		text1.style.display = "none";
/* 		document.getElementById("cat_vaccination").disabled = false;
		document.getElementById("cat_vaccination1").disabled = false;
		document.getElementById("dog_vaccination").disabled = false;
		document.getElementById("dog_vaccination1").disabled = false; */
		document.getElementById("cat_vaccination").checked  = false;
		document.getElementById("cat_vaccination1").checked  = false;
		document.getElementById("dog_vaccination").checked  = false;
		document.getElementById("dog_vaccination1").checked  = false;
		
  }
}
//Bk ends
</script>

</body>
</html>
