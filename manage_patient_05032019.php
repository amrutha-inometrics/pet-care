<?php
	ob_start();
	session_start();
	include_once("./includes/connection.php");
	include_once("./includes/functions.php");
	include_once "loginchk.inc.php";
	$a_name	= $_SESSION['a_name']; 
	$a_id	= $_SESSION['a_id']; 

		
	if(isset($_POST['subAdd']))
	{	
		$adtime = time();
		array_filter($_POST, 'trim_value');
		$postfilter =array(
			
			'txtpatientid'    	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtregfees'    	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'rgender'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'selspecies'   		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'selanimal'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'selbreed'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtname'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'selbday'     		=> array('filter' => FILTER_SANITIZE_NUMBER_INT, 'flags' => ''),
			'selbmonth'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'selbyear'     		=> array('filter' => FILTER_SANITIZE_NUMBER_INT, 'flags' => ''),
			'tovaccination'     => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtcolour'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtweight'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtchipno'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txthcardno'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'valid'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtowner'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtaddress'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtpincode'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtstate'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtphone'     		=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtmobile'     	=> array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
			'txtemail'     		=> array('filter' => FILTER_SANITIZE_EMAIL, 'flags' => ''),
		);
		$revised_post_array = filter_var_array($_POST, $postfilter);  
		$revised_post_array = sanitizearray($revised_post_array, $mysqli);
		
		if($_FILES['photo']['name'] != '') 
				{ 
					$updImg1     =    "files/temp/"; 
					$fileImg1    =    $_FILES['photo']['name']; 
					$updImg1     =    $updImg1.$fileImg1; 
					$ImgUpload1  =    move_uploaded_file($_FILES['photo']['tmp_name'],$updImg1); 
					$photo=resizeimage($updImg1,"files/photos/",$fileImg1,150,160);
		}else{ 
			$photo=""; 
		}
		$doj = strtotime(str_replace('/', '-',$_POST['txtdoj']));
		$fdov = strtotime(str_replace('/', '-',$_POST['txtfdov']));
		$dob = strtotime($_POST['selbday'].'-'.$_POST['selbmonth'].'-'.$_POST['selbyear']); 
		$qry_user="INSERT INTO p_patient(p_date,p_regfees,p_gender,p_name,p_species,p_animal,p_breed,p_dob,p_puppytype,p_firstvaccination,p_color,p_wieght,p_chipno,p_healthcard,p_valid,p_ownername,p_address,p_pincode,p_state,p_phone,p_mobile,p_email,p_photo,p_status,p_dateadded) VALUES('$doj','".$revised_post_array['txtregfees']."','".$revised_post_array['rgender']."','".$revised_post_array['txtname']."','".$revised_post_array['selspecies']."','".$revised_post_array['selanimal']."','".$revised_post_array['selbreed']."','$dob','".$revised_post_array['tovaccination']."','$fdov','".$revised_post_array['txtcolour']."','".$revised_post_array['txtweight']."','".$revised_post_array['txtchipno']."','".$revised_post_array['txthcardno']."','".$revised_post_array['valid']."','".$revised_post_array['txtowner']."','".$revised_post_array['txtaddress']."','".$revised_post_array['txtpincode']."','".$revised_post_array['txtstate']."','".$revised_post_array['txtphone']."','".$revised_post_array['txtmobile']."','".$revised_post_array['txtemail']."','$photo','1','$adtime')"; 
		$mysqli->query($qry_user) or die('Error, query failed');
		$lastid = $mysqli->insert_id; 
	 	$patientID = date('Y').$lastid; 
		
		$qry_update="UPDATE p_patient SET p_pid = '$patientID' WHERE p_id= '$lastid'"; 
		$mysqli->query($qry_update) or die('Error, query failed');
		
		$qry_vaccine="INSERT INTO p_vaccination(p_id,v_date) VALUES('$lastid','$fdov')"; 
		$mysqli->query($qry_vaccine) or die('Error, query failed');
		
		header ("location:manage_patient.php");
	}
	?>
<!DOCTYPE html>

<html>
<head>
<script src="includes/jquery-1.9.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
$('.ageclass').change(function(){
var dat = new Date();
var curday = dat.getDate();
var curmon = dat.getMonth()+1;
var curyear = dat.getFullYear();
var calday = document.getElementById("selbday").value;
var calmon = document.getElementById("selbmonth").value;
var calyear = document.getElementById("selbyear").value;
	if(curday == "" || curmon=="" || curyear=="" || calday=="" || calmon=="" || calyear=="")
	{
		return false;
	}	
	else
	{
		var curd = new Date(curyear,curmon-1,curday);
		var cald = new Date(calyear,calmon-1,calday);
		var diff =  Date.UTC(curyear,curmon,curday,0,0,0) - Date.UTC(calyear,calmon,calday,0,0,0);
		var dife = datediff(curd,cald);
		var agevalue1=dife[0];
		var agevalue2=dife[1];
		if(agevalue1==0){
			var agevalue=dife[1]+" months";
		}else {
			var agevalue=dife[0]+" year(s) "+ dife[1]+" month(s)";;
		}
	}
        $('#age').val(agevalue);
    });
});
function checkleapyear(datea)
{
	if(datea.getYear()%4 == 0)
	{
		if(datea.getYear()% 10 != 0)
		{
			return true;
		}
		else
		{
			if(datea.getYear()% 400 == 0)
				return true;
			else
				return false;
		}
	}
  return false;
}
function DaysInMonth(Y, M) {
    with (new Date(Y, M, 1, 12)) {
        setDate(0);
        return getDate();
    }
}
function datediff(date1, date2) {
    var y1 = date1.getFullYear(), m1 = date1.getMonth(), d1 = date1.getDate(),
	 y2 = date2.getFullYear(), m2 = date2.getMonth(), d2 = date2.getDate();
    if (d1 < d2) {
        m1--;
        d1 += DaysInMonth(y2, m2);
    }
    if (m1 < m2) {
        y1--;
        m1 += 12;
    }
    return [y1 - y2, m1 - m2, d1 - d2];
}
</script>
<?php include_once('header.php'); ?>
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
 <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
   <!-- Main Header -->
  <?php include_once('topbar.php'); ?>
  <!-- Left side column. contains the logo and sidebar -->
  
 <?php include_once('sidebar.php'); ?>
  <!-- Content Wrapper. Contains page content -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Manage Patient</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Manage Patient</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
	<div class="col-md-12">
	<div class="box box-primary collapsed-box"><!---->
            <div class="box-header with-border">
              <h3 class="box-title">Add Patient</h3>
			  <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
          </div>
            </div>
			

	  
            <!-- /.box-header -->
            <!-- form start -->
         <form class="form-horizontal" action=""  method="post" name="frm1" enctype="multipart/form-data">
              <div class="box-body">
			  
				<div class="form-group">
                  <label class="col-sm-4 control-label">Date of Joining *</label>
				   <div class="col-sm-8">
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input name="txtdoj" type="text" class="form-control pull-right" id="txtdoj" placeholder="Date of Joining" required>
				  
                </div>
				  </div>
                </div>
              <div class="form-group">
                  <label class="col-sm-4 control-label">Patient ID</label>
				   <div class="col-sm-8">
				   
                  <input type="text" name="txtpatientid" class="form-control" disabled="disabled" placeholder="Auto Generated" value="" >
				  </div>
           </div>
		   <div class="form-group">
                  <label class="col-sm-4 control-label">Registration Fees</label>
				   <div class="col-sm-8">
                  <input type="text" name="txtregfees" class="form-control"  placeholder="Enter Registration Fees">
				  </div>
           </div>
		   
			    <div class="form-group">
                  <label class="col-sm-4 control-label">Gender *</label>
				   <div class="col-sm-8">
                  
                      <input type="radio" name="rgender" id="rgender" value="Male" required> Male
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    
                      <input type="radio" name="rgender" id="rgender" value="Female" > Female
              
				  </div>
                </div>
				
				  <div class="form-group">
                  <label class="col-sm-4 control-label">Species *</label>
				   <div class="col-sm-8">
				   <select name="selspecies" class="form-control" id="species" required onChange="selectCategory(this.options[this.selectedIndex].value)" >
              <option value="" >Select Species</option>
			     <?php
                   $codemqry = "SELECT * FROM  p_category WHERE c_status ='1' AND c_parent = '0'  ORDER BY c_id ASC ";  
                    $codemres = $mysqli->query($codemqry);
                    while($codobj = $codemres->fetch_array())
					 {
									   ?>
                    <option value="<?php echo $codobj['c_id'] ?>"><?php echo $codobj['c_name'] ?></option>
                    <?php 	} ?>
            </select>
				  </div>
                </div>
				  <div class="form-group">
                  <label class="col-sm-4 control-label">Animal *</label>
				   <div class="col-sm-8">
				   <select id="category_dropdown" name="selanimal" class="form-control" required onChange="selectSubCategory(this.options[this.selectedIndex].value)">
                     <option value="" >Select Animal</option>
					 </select>
					 <span id="category_loader"></span>
					 </div>
					 </div>
                     <div class="form-group">
                     <label class="col-sm-4 control-label">Breed *</label>
                     <div class="col-sm-8">
				   <select id="subcategory_dropdown" name="selbreed" class="form-control">
                     <option value="" >Select Breed</option>
					 </select>
					  <span id="subcategory_loader"></span>
					 </div>
                     </div>
					 
                     <div class="form-group">
                     <label class="col-sm-4 control-label">
				     Name
				     </label>
                     <div class="col-sm-8">
                     <input type="text" name="txtname" class="form-control"  placeholder="Enter Name">
                     </div>
                     </div>
					 
                     <div class="form-group">
                     <label class="col-sm-4 control-label">
				     Date of Birth *
				     </label>
                     <div class="col-sm-8">
                     <select class="form-control col-sm-2 ageclass" style="width:150px;" name="selbday" id="selbday" required>
                     <option value="">Day</option>
                     <?php 
				for($i=1;$i<=31;$i++){
					if($i<10)
					$i	= "0".$i;
				?>
                     <option value="<?php echo $i?>"><?php echo $i;?></option>
                     <?php } ?>
                   </select>
				   <select class="form-control col-sm-2 ageclass" style="width:150px; margin-left:5px;" name="selbmonth" id="selbmonth" required>
                  <option value="">Month</option>
                  <option value="01">January</option>
                  <option value="02">February</option>
                  <option value="03">March</option>
                  <option value="04">April</option>
                  <option value="05">May</option>
                  <option value="06">June</option>
                  <option value="07">July</option>
                  <option value="08">August</option>
                  <option value="09">September</option>
                  <option value="10">October</option>
                  <option value="11">November</option>
                  <option value="12">December</option>
                </select>
                <select class="form-control col-sm-2 ageclass" style="width:150px; margin-left:5px;" name="selbyear" id="selbyear" required>
                  <option value="">Year</option>
                  <?php
				 	$cdate = 2000;
					for($x=date('Y');$x>=$cdate;$x--){
					?>
                  <option value="<?php echo $x?>"><?php echo $x;?></option>
                  <?php } ?>
                </select>
				
				  
                  <input type="text" id="age" name="txtage" class="form-control col-sm-2"  style="width:150px; margin-left:5px;" placeholder="Age" disabled="disabled">
				  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Type of Vaccination *</label>
				   <div class="col-sm-8">
                   <input type="radio" name="tovaccination" id="tovaccination" value="puppy" required> Puppy Vaccination
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    
                      <input type="radio" name="tovaccination" id="tovaccination" value="adult" > Adult Vaccination
				  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Date of First Vaccination *</label>
				   <div class="col-sm-8">
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input name="txtfdov" type="text" class="form-control pull-right" id="txtfdov" placeholder="Date of First Vaccination" required>
                </div>
				  </div>
                </div>
				
				<div class="form-group">
                  <label class="col-sm-4 control-label">Colour</label>
				   <div class="col-sm-8">
                  <input name="txtcolour" type="text" class="form-control"  placeholder="Enter Colour">
				  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Weight</label>
				   <div class="col-sm-8">
                  <input name="txtweight" type="text" class="form-control"  placeholder="Enter Weight">
				  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Chip No</label>
				   <div class="col-sm-8">
                  <input name="txtchipno" type="text" class="form-control"  placeholder="Enter Chip No">
				  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Health Card No</label>
				   <div class="col-sm-8">
                  <input name="txthcardno" type="text" class="form-control"  placeholder="Enter Health Card No">
				  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Valid</label>
				   <div class="col-sm-8">
                  
                      <input type="radio" name="valid" id="valid" value="Yes"> Yes
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    
                      <input type="radio" name="valid" id="valid" value="No"> No
              
				  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Owner Name</label>
				   <div class="col-sm-8">
                  <input name="txtowner" type="text" class="form-control" placeholder="Enter Name of Owner">
				  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-4 control-label">Address</label>
				   <div class="col-sm-8">
				  <textarea name="txtaddress" class="form-control "  rows="3" placeholder="Enter Address of Owner"></textarea>
                </div>
		   </div>
				
					<div class="form-group">
                  <label class="col-sm-4 control-label">Pincode</label>
				   <div class="col-sm-8">
                  <input name="txtpincode" type="text" class="form-control" placeholder="Enter Pincode">
				  </div>
                </div>


					<div class="form-group">
                  <label class="col-sm-4 control-label">State</label>
				   <div class="col-sm-8">
                  <input name="txtstate" type="text" class="form-control" placeholder="Enter State">
				  </div>
                </div>

				  <div class="form-group">
                  <label class="col-sm-4 control-label">Phone Number</label>
				   <div class="col-sm-8">
                  <input type="text" name="txtphone" class="form-control  col-sm-4"   placeholder="Enter Phone Number">
				  </div>
                </div>
				
				  <div class="form-group">
                  <label class="col-sm-4 control-label">Mobile Number *</label>
				   <div class="col-sm-8">
                  <input type="text" name="txtmobile" class="form-control  col-sm-4" required placeholder="Enter Mobile Number">
				  </div>
                </div>
				
			
				<div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-4 control-label">Email address</label>
				   <div class="col-sm-8">
                  <input name="txtemail" type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Email address">
				  </div>
                </div>
				
                <div class="form-group">
                  <label class="col-sm-4 control-label">Photo</label>
				   <div class="col-sm-8">
                  <input type="file" id="exampleInputFile"  name="photo">
				  </div>

                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="subAdd" class="btn btn-primary">Submit</button>
              </div>
      </form>
          </div>
	</div>

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
			<div class="box-header">
              <h3 class="box-title">Manage Patient</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>ID</th>
					<th>Name</th>
					<th>Owner Name</th>
					<th>Email</th>
					<th>Mobile</th>
					<th>Vaccination</th>
                    <th>Edit</th>
					<th>Print</th>
                    <th>Status</th>					
                    <th>Delete</th> 
                  </tr>
                </thead>
                <tbody>
                  <?php 
					$qry=$mysqli->query("SELECT * FROM p_patient  ORDER BY p_id DESC");
					while($cin = mysqli_fetch_array($qry)){
					?>
                  <tr>
                    <td><?php echo $cin['p_pid'];?></td>
                    <td><?php echo $cin['p_name'];?></td>
					<td><?php echo $cin['p_ownername'];?></td>
					<td><?php echo $cin['p_email'];?></td>
					<td><?php echo $cin['p_mobile'];?></td>
					<td><a href="manage_vaccination.php?pid=<?php echo $cin['p_id'] ?>" title="Edit" style="color: #444;"><i class="fa fa-sliders" style="font-size: 17px;"></i> Manage</a></td>
					
                    
                    <td><a href="edit_patient.php?id=<?php echo $cin['p_id'] ?>" title="Edit" style="color: #444;"><i class="fa fa-pencil-square" style="font-size: 17px;"></i></a></td>
					<td><a href="print_patient.php?id=<?php echo $cin['p_id'] ?>" target="_blank" title="Print" style="color: #444;"><i class="fa fa-print" style="font-size: 17px;"></i></a></td>
					<td>
					<?php if($cin['p_status']=='1')  {?><a href="status_patient.php?id=<?php echo $cin['p_id'] ?>&act=0" title="Make it Inactive" style="color: green;"><i class="fa fa-check-circle" style="font-size: 17px;"></i></a>
					<?php } else {?><a href="status_patient.php?id=<?php echo $cin['p_id'] ?>&act=1" title="Make it Active" style="color: red;"><i class="fa fa-times-circle" style="font-size: 17px;"></i></a>
					<?php } ?>  
					</td>
					<td><a href="delete_patient.php?id=<?php echo $cin['p_id'] ?>" onClick="return confirm('Are you sure you want to delete?');" title="Delete" style="color: #444;"><i class="fa fa-trash" style="font-size: 17px;"></i></a></td>
				
                  </tr>
                  <?php }
					?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->
  <!-- Main Footer -->
   <?php include_once('footer.php');?>
  <div class="control-sidebar-bg"></div>
</div>
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#txtdoj').datepicker();
	$('#txtfdov').datepicker();
	$('#example1').dataTable( {
		"ordering": false,
		scrollX : true,
        scrollCollapse : true,
		responsive: true,
        autoWidth: false,
  "columnDefs": [
		{ "width": "4%", "targets": [4,5,6,7,8] },
		{ "width": "20%", "targets": [0,1,2,3] },
	]
	});
  });
   $(document).ready(function() {
	$('a[data-confirm]').click(function(ev) {
		var href = $(this).attr('href');
		if (!$('#dataConfirmModal').length) {
			$('body').append('<div id="dataConfirmModal" class="modal" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button><h3 id="dataConfirmLabel">Please Confirm</h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button><a class="btn btn-primary" id="dataConfirmOK">OK</a></div></div>');
		} 
		$('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
		$('#dataConfirmOK').attr('href', href);
		$('#dataConfirmModal').modal({show:true});
		return false;
	});
});
</script>
<script type="text/javascript">

function selectCategory(c_id){

	if(c_id!=""){
		loadData('category',c_id);
		//loadData('subcategory',c_id);
	}else{
		$("#category_dropdown").html("<option value=''>Select Animal</option>");
	}
}

function selectSubCategory(c_id){

	if(c_id!=""){
		loadData('subcategory',c_id);
	}else{
		$("#subcategory_dropdown").html("<option value=''>Select Breed</option>");		
	}
}

function loadData(loadType,loadId){
	var dataString = 'loadType='+ loadType +'&loadId='+ loadId;
	$("#"+loadType+"_loader").show();
    $("#"+loadType+"_loader").fadeIn(400).html('<img src="images/loader.gif" style="padding: 5px; margin-left: 5px;" />');
	$.ajax({
		type: "POST",
		url: "loadCategory.php",
		data: dataString,
		cache: false,
		success: function(result){
			$("#"+loadType+"_loader").hide();
			$("#"+loadType+"_dropdown").html(result);  
		}
	});
}
</script>
</body>
</html>