<?php
class Registration
{
    /**
     * @var object $db_connection The database connection
     */
    private $db_connection            = null;
    /**
     * @var bool success state of registration
     */
    public  $registration_successful  = false;
    /**
     * @var bool success state of verification
     */
    public  $verification_successful  = false;
    /**
     * @var array collection of error messages
     */
    public  $errors                   = array();
    /**
     * @var array collection of success / neutral messages
     */
    public  $messages                 = array();

    /**
     * the function "__construct()" automatically starts whenever an object of this class is created,
     * you know, when you do "$login = new Login();"
     */
    public function __construct()
    {
        session_start();

        // if we have such a POST request, call the registerNewUser() method
        if (isset($_POST["register"])) { 
            $this->registerNewUser($_POST['seltype'],$_POST['fullname'],$_POST['user_email'], $_POST['user_password'],
                                   $_POST['address'], $_POST['mobnumber'], $_POST['landnumber'],$_POST['selsp'],$_POST['companyname'],$_POST['contactperson'],$_POST['contactnumber']);
        // if we have such a GET request, call the verifyNewUser() method
        } else if (isset($_GET["activationid"]) && isset($_GET["verification_code"])) {
            $this->verifyNewUser($_GET["activationid"], $_GET["verification_code"]);
        }
    }

    /**
     * Checks if database connection is opened and open it if not
     */
    private function databaseConnection()
    {
        // connection already opened
        if ($this->db_connection != null) {
            return true;
        } else {
            // create a database connection, using the constants from config/config.php
            try {
                // Generate a database connection, using the PDO connector
                // @see http://net.tutsplus.com/tutorials/php/why-you-should-be-using-phps-pdo-for-database-access/
                // Also important: We include the charset, as leaving it out seems to be a security issue:
                // @see http://wiki.hashphp.org/PDO_Tutorial_for_MySQL_Developers#Connecting_to_MySQL says:
                // "Adding the charset to the DSN is very important for security reasons,
                // most examples you'll see around leave it out. MAKE SURE TO INCLUDE THE CHARSET!"
                $this->db_connection = new PDO('mysql:host='. DB_HOST .';dbname='. DB_NAME . ';charset=utf8', DB_USER, DB_PASS);
                return true;
            // If an error is catched, database connection failed
            } catch (PDOException $e) {
                $this->errors[] = MESSAGE_DATABASE_ERROR;
                return false;
            }
        }
    }

    /**
     * handles the entire registration process. checks all error possibilities, and creates a new user in the database if
     * everything is fine
     */
    private function registerNewUser($type,$fullname, $user_email, $user_password, $address, $mobnumber, $landnumber, $sp, $companymane, $contactperson, $contactnumber)
    {
	
        // we just remove extra space on username and email
        $user_email = trim($user_email);
        $mobnumber  = trim($mobnumber);
        $fullname   = trim($fullname);

 	if ($this->databaseConnection()) {
		
            // check if username or email already exists
            $query_check_user_name = $this->db_connection->prepare('SELECT u_user_email FROM h_users WHERE u_user_email=:user_email');
            $query_check_user_name->bindValue(':user_email', $user_email, PDO::PARAM_STR);
            $query_check_user_name->execute();
            $result = $query_check_user_name->fetchAll();

            // if username or/and email find in the database
            // TODO: this is really awful!
            if (count($result) > 0) {
                for ($i = 0; $i < count($result); $i++) {
                    $this->errors[] = ($result[$i]['u_user_email'] == $user_email) ? MESSAGE_EMAIL_ALREADY_EXISTS : MESSAGE_USERNAME_EXISTS;
                }
            } else {
			
                // check if we have a constant HASH_COST_FACTOR defined (in config/hashing.php),
                // if so: put the value into $hash_cost_factor, if not, make $hash_cost_factor = null
                $hash_cost_factor = (defined('HASH_COST_FACTOR') ? HASH_COST_FACTOR : null);

                // crypt the user's password with the PHP 5.5's password_hash() function, results in a 60 character hash string
                // the PASSWORD_DEFAULT constant is defined by the PHP 5.5, or if you are using PHP 5.3/5.4, by the password hashing
                // compatibility library. the third parameter looks a little bit shitty, but that's how those PHP 5.5 functions
                // want the parameter: as an array with, currently only used with 'cost' => XX.
                $user_password_hash = password_hash($user_password, PASSWORD_DEFAULT, array('cost' => $hash_cost_factor));
                // generate random hash for email verification (40 char string)
                $user_activation_hash = sha1(uniqid(mt_rand(), true));
                // write new users data into database
				$modtime = time();
                $query_new_user_insert = $this->db_connection->prepare('INSERT INTO h_users (u_user_email, u_user_password_hash, u_user_activation_hash, u_user_registration_ip, u_type, u_name, u_address,  u_mobno, u_landno,u_serviceprovider, u_companyname, u_contactperson, u_contactnumber, u_user_registration_datetime) VALUES(:user_email, :user_password_hash, :user_activation_hash, :user_registration_ip, :u_type, :u_name, :u_address, :u_mobno, :u_landno, :u_serviceprovider,  :u_companyname, :u_contactperson, :u_contactnumber, now())');
				
			  	$query_new_user_insert->bindValue(':u_type', $type, PDO::PARAM_STR);
                $query_new_user_insert->bindValue(':user_email', $user_email, PDO::PARAM_STR);
                $query_new_user_insert->bindValue(':user_password_hash', $user_password_hash, PDO::PARAM_STR);
                $query_new_user_insert->bindValue(':user_activation_hash', $user_activation_hash, PDO::PARAM_STR);
                $query_new_user_insert->bindValue(':user_registration_ip', $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
                $query_new_user_insert->bindValue(':u_name', $fullname, PDO::PARAM_STR);
				$query_new_user_insert->bindValue(':u_address', $address, PDO::PARAM_STR);
                $query_new_user_insert->bindValue(':u_mobno', $mobnumber, PDO::PARAM_STR);
				$query_new_user_insert->bindValue(':u_landno', $landnumber, PDO::PARAM_STR);
				$query_new_user_insert->bindValue(':u_serviceprovider', $sp, PDO::PARAM_STR);
				$query_new_user_insert->bindValue(':u_companyname', $companymane, PDO::PARAM_STR);
				$query_new_user_insert->bindValue(':u_contactperson', $contactperson, PDO::PARAM_STR);
				$query_new_user_insert->bindValue(':u_contactnumber', $contactnumber, PDO::PARAM_STR);
				
				$query_new_user_insert->execute();

                // id of new user
                $user_id = $this->db_connection->lastInsertId();

		     if ($query_new_user_insert) {
                    // send a verification email
                    if ($this->sendVerificationEmail($user_id, $user_email, $user_activation_hash)) {
                        // when mail has been send successfully
                        
                        $this->messages[] = MESSAGE_VERIFICATION_MAIL_SENT;
                        $this->registration_successful = true;
                    }   /*else {
                        // delete this users account immediately, as we could not send a verification email
                        $query_delete_user = $this->db_connection->prepare('DELETE FROM b_users WHERE userid=:user_id');
                        $query_delete_user->bindValue(':user_id', $user_id, PDO::PARAM_INT);
                        $query_delete_user->execute();

                        $this->errors[] = MESSAGE_VERIFICATION_MAIL_ERROR;
                    }*/
                } else {
                    $this->errors[] = MESSAGE_REGISTRATION_FAILED;
                }
				
				
				
				
            }
        }
    }

    /*
     * sends an email to the provided email address
     * @return boolean gives back true if mail has been sent, gives back false if no mail could been sent
     */
    public function sendVerificationEmail($user_id, $user_email, $user_activation_hash)
    {
        $mail = new PHPMailer;

        // please look into the config/config.php for much more info on how to use this!
        // use SMTP or use mail()
        if (EMAIL_USE_SMTP) {
            // Set mailer to use SMTP
            $mail->IsSMTP();
            //useful for debugging, shows full SMTP errors
            //$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
            // Enable SMTP authentication
            $mail->SMTPAuth = EMAIL_SMTP_AUTH;
            // Enable encryption, usually SSL/TLS
            if (defined(EMAIL_SMTP_ENCRYPTION)) {
                $mail->SMTPSecure = EMAIL_SMTP_ENCRYPTION;
            }
            // Specify host server
            $mail->Host = gethostbyname(EMAIL_SMTP_HOST);
            $mail->Username = EMAIL_SMTP_USERNAME;
            $mail->Password = EMAIL_SMTP_PASSWORD;
            $mail->Port = EMAIL_SMTP_PORT;
        } else {
            $mail->IsMail();
        }
        $mail->IsHTML(true);
        $mail->From = EMAIL_VERIFICATION_FROM;
        $mail->FromName = EMAIL_VERIFICATION_FROM_NAME;
        $mail->AddAddress($user_email);
        $mail->Subject = EMAIL_VERIFICATION_SUBJECT;

        $link = EMAIL_VERIFICATION_URL.'customer-register.php?activationid='.urlencode($user_id).'&verification_code='.urlencode($user_activation_hash);
		$link1 = EMAIL_VERIFICATION_URL;
        // the link to your register.php, please set this value in config/email_verification.php
        //$mail->Body = EMAIL_VERIFICATION_CONTENT.' '.$link;
        
         $mail->Body = '<div style="width:100%; height:auto; min-width:750px;box-shadow: 0 5px 10px #ededed; margin:auto;font-family:Tahoma, sans-serif;"> <div style="width:100%; height:auto;">
    <div style="width:100%; height:130px;">
      <div style="float:left; margin:20px 0 0 0;"><img src="http://www.homeandspaces.com/images/LOGO.jpg" width="340" height="103" alt=""/></div>
      
      <div style="font-family:Tahoma, sans-serif; font-size:18px; color:#000000; float:right; margin:2px 0 0 0px; clear:right;"></div>
    </div> <div style="height:auto; font-size:14px; color:#000000; margin:0 0 0 0; line-height:20px;">
      <h1 style="font-size:18px; color:#eb0000; margin:18px 0 20px 0px;">Hi,</h1>
	     <p style="color: #666; font-size: 10px;" >This mail is sent to you by Home And Spaces as you have registered your membership with us. For assuring delivery of our mails into your inbox, please save the address info@homeandspaces.com </p>
	  
      <p style="font-weight: bold;">Welcome to Home And Spaces. </p>
	
          <p>Please confirm your email address by clicking on following button:</p>
        <br />
        <p align="center"><a href="'.$link.'" style="color: #fff; text-decoration: none; border-radius: 2px; background-color: #eb0000; font-size: 16px; font-weight: bold; border: 8px solid #eb0000;" >Confirm my email</a></p>
        <br />
		<p style="color: #666; font-size: 10px;" >If you did not request any registration, ignore this message and nothing will happen.</p>
                <p>After successful activation of your membership you can post, edit or delete your property. </p>
		
    <div style="font-size:14px; color:#000000;margin-top:32px;width:400px;float:left;line-height:20px;">
      Team Home And Spaces<br /></div>
    <div style="clear:both;"></div>
    <div style="clear:both;"></div>
    <div >
      <div style="font-size:13px; color:#000000;margin:0 32px 40px 0px;float:right;line-height:20px;"> Home And Spaces,<br />
         AB Centre, 39/119A,Mullassery Canal Road,Karikkamuri Cross Road,<br />
        Cochin-11, Kerala, India<br />
        E-mail: hello@homeandspaces.com<br />
        Ph: +91-484-3273904, +91-9061751852<br />
        www.homeandspaces.com.<br />
      </div>
    </div>
  </div>
</div>';

        if(!$mail->Send()) {
            $this->errors[] = MESSAGE_VERIFICATION_MAIL_NOT_SENT . $mail->ErrorInfo;
            return false;
        } else {
            return true;
        }
    }

    /**
     * checks the id/verification code combination and set the user's activation status to true (=1) in the database
     */
    public function verifyNewUser($user_id, $user_activation_hash)
    {
        // if database connection opened
        if ($this->databaseConnection()) {
            // try to update user with specified information
        
            $query_update_user = $this->db_connection->prepare('UPDATE h_users SET u_user_active = 1, u_user_activation_hash = NULL WHERE userid = :user_id AND u_user_activation_hash = :user_activation_hash');
          
            $query_update_user->bindValue(':user_id', intval(trim($user_id)), PDO::PARAM_INT);
            $query_update_user->bindValue(':user_activation_hash', $user_activation_hash, PDO::PARAM_STR);
            $query_update_user->execute();

            if ($query_update_user->rowCount() > 0) {
                
	            unset($this->errors);
                $this->verification_successful = true;
                $this->messages[] = MESSAGE_REGISTRATION_ACTIVATION_SUCCESSFUL;
            } else {
                unset($this->errors);
                $this->acterrors[] = MESSAGE_REGISTRATION_ACTIVATION_NOT_SUCCESSFUL;
            }
        }
    }
}
