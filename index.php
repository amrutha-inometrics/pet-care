<?php
	ob_start();
	session_start();
	include_once("./includes/connection.php");
	include_once("./includes/functions.php");
	if(isset($_SESSION['a_name']) || $_SESSION['a_name']!="" || isset($_SESSION['a_id']) || $_SESSION['a_id']!="")
	{header("location:manage_patient.php");}
	$msg	 	= 	isset($_REQUEST['msg'])	?	$_REQUEST['msg']	:	"";
	if(isset($_POST['Submit']))
	{
		array_filter($_POST, 'trim_value');
		$postfilter =array(
		'txtLogin'   	 => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
		'txtPassword'    => array('filter' => FILTER_SANITIZE_STRING, 'flags' => FILTER_FLAG_STRIP_LOW),
		);
		$revised_post_array = filter_var_array($_POST, $postfilter);  
		$revised_post_array = sanitizearray($revised_post_array, $mysqli);
		
		if (!isset($username) || $username == "")
			{
				$msg	= "Please enter username!";
				header("location:index.php?msg=$msg");
			}
		if (!isset($password) || $password == "")
			{
				$msg	= "Please enter password!";
				header("location:index.php?msg=$msg");
			}
			$pass = md5($revised_post_array['txtPassword']);
			
		$result_query = $mysqli->query("select * from p_admin where admin_email='".$revised_post_array['txtLogin']."' AND admin_pwd='$pass' ");
		$num_rows		= mysqli_num_rows($result_query);
		if($num_rows>0)
		{
			$result_row	= $result_query->fetch_array();
			$_SESSION['a_name']	= $result_row['admin_name'];
			$_SESSION['a_id']	= $result_row['admin_id'];
			$_SESSION['a_role']	= $result_row['admin_role']; 
			header("location:manage_patient.php");
		}
		else
		{
			$msg="Invalid UserName or Password!";
			header("location:index.php?msg=$msg");
		}
	}
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>The Cochin Pet Hospital</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box" style="margin: 15% auto;">
<!--  <div class="login-logo">
    <a href="./"><b>Evolution </b>Admin</a>
  </div>-->
  <!-- /.login-logo -->
  <div class="login-box-body">
    <div class="login-logo"><img src="images/logo.png"></div>
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="text" name="txtLogin" class="form-control" placeholder="Username">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="txtPassword" type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button name="Submit" type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

<!--    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div>
-->    <!-- /.social-auth-links -->

    <!--<a href="#">I forgot my password</a><br>
    <a href="register.html" class="text-center">Register a new membership</a>-->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<?php include_once('footer.php') ?>
<style>
.main-footer {
margin: 0;
}
</style>
<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
